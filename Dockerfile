# *****************************************************************
#                                                                 *
# This is universal dockerfile for any bot created by botmanteam  *
#                                                                 *
# *****************************************************************

FROM registry.gitlab.com/botman_bots/botmanlib:1.8.4-alpine
MAINTAINER Flowelcat <flowelcat@gmail.com>

# Copying sources to image and installing bot dependencies
RUN mkdir /bot
COPY . /bot
WORKDIR /bot/
RUN pip install -r requirements.txt
RUN pip install -e .
RUN mv /bot/resources /bot/bot_resources

# Initializing botmanlib
RUN botmanlib init

# Creating command for starting container
CMD cp -r /bot/bot_resources/* /bot/resources;botmanlib startbot
