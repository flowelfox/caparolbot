import enum

from botmanlib.menus.helpers import get_settings, to_state, write_settings
from botmanlib.menus.list_menus.one_list_menu import OneListMenu
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, ProductDecorativeCoatings
from src.settings import SETTINGS_FILE


class DecorativeTechnicsMenu(OneListMenu):
    menu_name = 'decorative_technics_menu'

    class States(enum.Enum):
        ACTION = 1
        PRODUCT = 2

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^technics$')]

    def back(self, update, context):
        self.parent.send_message(context)
        return self.parent.States.ACTION

    def query_objects(self, user_data):
        settings = get_settings(SETTINGS_FILE)

        return settings["technics"]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            message_text = _("Select an action, {name}.").format(name=user.desired_name)
        else:
            message_text = _("There is nothing to list\n")

        return message_text

    def send_message(self, context):
        user_data = context.user_data
        user = user_data['user']

        back_button = self.back_button(context)

        if user_data[self.menu_name]['filtered_objects']:
            back_button = self.search_back_button(context)

        objects = self._get_objects(context)

        buttons = []
        before_buttons = self.top_buttons(context)
        if before_buttons:
            buttons.extend(before_buttons)

        if not objects:
            message_text = self.message_text(context, None)
            controls = self.controls(context)
            if controls:
                buttons.append(controls)

            object_buttons = self.object_buttons(context, [])
            if object_buttons:
                buttons.extend(object_buttons)
        else:
            selected_object = objects[user_data[self.menu_name]['selected_object']]

            message_text = self.message_text(context, selected_object)

            controls = self.controls(context, o=selected_object)
            if controls:
                buttons.append(controls)

            object_buttons = self.object_buttons(context, selected_object)
            if object_buttons:
                buttons.extend(object_buttons)

            delete_button = self.delete_button(context)
            if self.add_delete_button and delete_button:
                buttons.append([delete_button])

            message_text += self.page_text(user_data[self.menu_name]['selected_object'] + 1, user_data[self.menu_name]['max_page'], context)

        if back_button:
            buttons.append([back_button])

        after_buttons = self.bottom_buttons(context)
        if after_buttons:
            buttons.extend(after_buttons)

        markup = InlineKeyboardMarkup(buttons)

        try:
            mes = send_or_edit(context, chat_id=user.chat_id, photo=objects[user_data[self.menu_name]['selected_object']]["file"], reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        except TelegramError as e:
            if e.message.startswith("Can't parse entities"):
                message_text = message_text.replace('\\', "")
                mes = send_or_edit(context, chat_id=user.chat_id, photo=objects[user_data[self.menu_name]['selected_object']]["file"], reply_markup=markup, parse_mode=None, disable_web_page_preview=self.disable_web_page_preview)
            else:
                raise e

        obj = self.selected_object(context)
        if 'http' in obj["file"]:
            obj["file"] = mes.photo[-1].file_id
            write_settings({"technics": objects}, SETTINGS_FILE)

        return mes

    def object_buttons(self, context, obj):
        _ = context.user_data['_']
        buttons = []
        if obj["product"] is not None:
            buttons.append([InlineKeyboardButton(_("Show product"), callback_data='show_product')])
        return buttons

    def go_to_product(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        prod = DBSession.query(ProductDecorativeCoatings).filter(ProductDecorativeCoatings.language_code == user.language_code).filter(ProductDecorativeCoatings.name == self.selected_object(context)["product"]).first()
        if prod is None:
            context.bot.answer_callback_query(update.callback_query.id, text=_("There is no such product yet"), show_alert=True)
            return self.States.ACTION
        update.callback_query.data = f"id{prod.id}"
        self.parent.decorative_coats_menu.show_product(update, context)
        return self.States.PRODUCT

    def back_to_list(self, update, context):
        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def right_state(self, func):
        def inner_func(update, context):
            func(update, context)
            return self.States.PRODUCT

        return inner_func

    def additional_states(self):

        return {self.States.ACTION: [CallbackQueryHandler(self.back_to_list, pattern='^menu$'),
                                     CallbackQueryHandler(self.go_to_product, pattern='^show_product$')],
                self.States.PRODUCT: [CallbackQueryHandler(self.go_to_product, pattern='^menu$'),
                                      CallbackQueryHandler(self.right_state(self.parent.decorative_coats_menu.show_picture), pattern='^picture$'),
                                      CallbackQueryHandler(self.right_state(self.parent.decorative_coats_menu.send_doc), pattern='^ti$'),
                                      CallbackQueryHandler(self.right_state(self.parent.decorative_coats_menu.show_features), pattern='^features_\d+$'),
                                      CallbackQueryHandler(self.right_state(self.parent.decorative_coats_menu.show_characteristics), pattern='^characteristics_\d+$'),
                                      CallbackQueryHandler(self.back_to_list, pattern='^back_to_list$'),
                                      MessageHandler(Filters.all, to_state(self.States.PRODUCT))]
                }
