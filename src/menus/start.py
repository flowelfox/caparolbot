import enum

import formencode
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, translator, unknown_command, to_state, generate_regex_handlers, get_settings, write_settings, prepare_user
from botmanlib.messages import delete_interface, send_or_edit, remove_interface_markup
from emoji import emojize
from formencode import validators, Invalid
from telegram import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CommandHandler, CallbackQueryHandler, Filters, MessageHandler, PrefixHandler

from src.menus.colored_solutions2021 import ColoredSolutionsMenu
from src.menus.contacts import ContactsMenu
from src.menus.decorative_technics import DecorativeTechnicsMenu
from src.menus.decorative_types import DecorativeCoatingsMenu
from src.menus.first_coats import FirstCoatsMenu
from src.menus.health_and_safety import HealthAndSafetyMenu
from src.menus.heat_systems import HeatSystemsMenu
from src.menus.paints import PaintsMenu
from src.menus.putty_and_plaster import PuttyAndPlasterMenu
from src.models import User
from src.settings import CHAT_LINK, SETTINGS_FILE, STICKER_PACK_LINK


class StartMenu(BaseMenu):
    menu_name = 'start_menu'

    class States(enum.Enum):
        ACTION = 1
        NAME = 2
        CONFIRM = 3
        LANGUAGE = 4
        ECO = 5
        DECORATIVE = 6
        HEAT = 7
        PRODUCT = 8

    def entry(self, update, context):
        user = prepare_user(User, update, context, lang='uk')

        context.user_data['user'] = user
        _ = context.user_data['_'] = translator(user.language_code)
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.message and 'start' in update.message.text:
            delete_interface(context)
            delete_interface(context, interface_name='language_interface')

        if update.message and 'Main menu' in update.message.text:
            self.clear_all_states(context)

        if update.callback_query and update.callback_query.data == 'start':
            remove_interface_markup(context)
        buttons = [[KeyboardButton(_("Main menu"))]]
        if user.desired_name is None:
            send_or_edit(context, chat_id=user.chat_id, text=_("How can I call you?") + emojize(":slightly_smiling_face:"), reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True))

            return self.States.NAME
        else:
            send_or_edit(context, interface_path='language_interface', chat_id=user.chat_id,
                         text=_("Welcome, {name}!").format(name=user.desired_name), reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True))
            delete_interface(context)
            self.send_message(context)

            return self.States.ACTION

    def set_name(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        text = update.effective_message.text

        try:
            val = formencode.Pipe(validators.String())
            user.desired_name = val.to_python(text)

            buttons = [[InlineKeyboardButton(_("Yes"), callback_data="yes"), InlineKeyboardButton(_("No"), callback_data="no")],
                       [InlineKeyboardButton(_("Back"), callback_data="back")]]
            send_or_edit(context, chat_id=user.chat_id,
                         text=_("I will call you {name}").format(name=user.desired_name) + emojize(":winking_face:"),
                         reply_markup=InlineKeyboardMarkup(buttons))
            return self.States.CONFIRM

        except Invalid:
            send_or_edit(context, chat_id=user.chat_id, text=_("You entered wrong name. Try again."), resize_keyboard=True)
            return self.States.NAME

    def confirm_name(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not add_to_db(user):
            return self.conv_fallback(context)

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Ecological products"), callback_data='ecological_products')],
                   [InlineKeyboardButton(_("Decorative coatings"), callback_data='technics')],
                   [InlineKeyboardButton(_("Colored and trend solutions"), callback_data='colored_solutions')],
                   [InlineKeyboardButton(_("Heat systems"), callback_data='heat')],
                   [InlineKeyboardButton(_("Safety and health"), callback_data='safety_and_health')],
                   [InlineKeyboardButton(_("Contacts, Your nearest center"), callback_data='contacts')],
                   [InlineKeyboardButton(_("Chat with colleagues"), url=CHAT_LINK)],
                   [InlineKeyboardButton(_("Caparol Profi stickers"), url=STICKER_PACK_LINK)],
                   [InlineKeyboardButton(emojize(":globe_with_meridians:") + _("Change language"), callback_data='сhange_language')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[emojize(":Ukraine:") + "Українська", emojize(":Russia:") + "Русский"]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Choose language:"), reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True))
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        try:
            val = formencode.Pipe(validators.String(), validators.OneOf([emojize(":Ukraine:") + "Українська", emojize(":Russia:") + "Русский"]))
            value = val.to_python(update.message.text)

            if value == emojize(":Ukraine:") + "Українська":
                user.language_code = 'uk'
            elif value == emojize(":Russia:") + "Русский":
                user.language_code = 'ru'
            else:
                user.language_code = 'uk'

            add_to_db(user)
            context.user_data['_'] = _ = translator(user.language_code)

            # send_or_edit(context, chat_id=user.chat_id, text=_("Language changed"))

            return self.entry(update, context)

        except Invalid:
            delete_interface(context)
            context.bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"))
            return self.ask_language(update, context)

    def back_to_menu(self, update, context):
        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def eco_products(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Paints"), callback_data='paints')],
                   [InlineKeyboardButton(_("Putty"), callback_data='typep_putty'), InlineKeyboardButton(_("Plaster"), callback_data='typep_plaster')],
                   [InlineKeyboardButton(_("First Coats"), callback_data='first_coats')],
                   [InlineKeyboardButton(_("Decorative products"), callback_data='dec_types')],
                   [InlineKeyboardButton(_("Insulation facades"), callback_data='in_fa')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ECO

    def heat_systems(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Capatect Carbon"), callback_data='heat_carbon')],
                   [InlineKeyboardButton(_("Capatect Longlife"), callback_data='heat_longlife')],
                   [InlineKeyboardButton(_("Capatect Classic"), callback_data='heat_classic')],
                   [InlineKeyboardButton(_("Capatect Standart"), callback_data='heat_standart')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_eco')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ECO

    def show_heat(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Capatect Carbon"), callback_data='heat_carbon')],
                   [InlineKeyboardButton(_("Capatect Longlife"), callback_data='heat_longlife')],
                   [InlineKeyboardButton(_("Capatect Classic"), callback_data='heat_classic')],
                   [InlineKeyboardButton(_("Capatect Standart"), callback_data='heat_standart')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.HEAT

    def show_heat_info(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("To products"), callback_data='to_product')],
                   [InlineKeyboardButton(_("Show scheme"), callback_data='scheme')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_heat')]]
        if update.callback_query.data == "heat_carbon":
            context.user_data["heat_type"] = "Capatect CARBON"
        elif update.callback_query.data == "heat_longlife":
            context.user_data["heat_type"] = "Capatect LONGLIFE"
        elif update.callback_query.data == "heat_classic":
            context.user_data["heat_type"] = "Capatect CLASSIC"
        elif update.callback_query.data == "heat_standart":
            context.user_data["heat_type"] = "Capatect STANDARD"
        else:
            if context.user_data["heat_type"] == "Capatect STANDARD":
                update.callback_query.data = "heat_standart"
            if context.user_data["heat_type"] == "Capatect CLASSIC":
                update.callback_query.data = "heat_classic"
            if context.user_data["heat_type"] == "Capatect LONGLIFE":
                update.callback_query.data = "heat_longlife"
            if context.user_data["heat_type"] == "Capatect CARBON":
                update.callback_query.data = "heat_carbon"

        send_or_edit(context, chat_id=user.chat_id, text=settings["heat"][language_code][update.callback_query.data.replace("heat_", "")], reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.HEAT

    def show_scheme(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        data = "scheme_" + context.user_data["heat_type"].replace('Capatect ', '').lower()
        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="menu")]]
        settings = get_settings(SETTINGS_FILE)

        interface = send_or_edit(context, chat_id=user.chat_id, photo=settings["heat_photos"][data], reply_markup=InlineKeyboardMarkup(buttons))

        if 'http' in settings["heat_photos"][data]:
            settings["heat_photos"][data] = interface.photo[-1].file_id
            write_settings({"heat_photos": settings["heat_photos"]}, SETTINGS_FILE)

        return self.States.HEAT

    def paints_types(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Interior paints"), callback_data='type_interior')],
                   [InlineKeyboardButton(_("Facade paints"), callback_data='type_facade')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_eco')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ECO

    def right_state(self, func):
        def inner_func(update, context):
            func(update, context)
            return self.States.PRODUCT

        return inner_func

    def goto_admin(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        contacts_menu = ContactsMenu(self)
        colored_solutions_menu = ColoredSolutionsMenu(self)
        health_menu = HealthAndSafetyMenu(self)
        paints_menu = PaintsMenu(self)
        first_coats_menu = FirstCoatsMenu(self)
        putty_and_plaster_menu = PuttyAndPlasterMenu(self)
        self.decorative_coats_menu = DecorativeCoatingsMenu(self)
        decorative_technics_menu = DecorativeTechnicsMenu(self)
        heat_systems_menu = HeatSystemsMenu(self)

        handler = ConversationHandler(entry_points=generate_regex_handlers("Main menu", self.entry) +
                                                   [PrefixHandler('/', 'start', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern='^start$')],
                                      states={self.States.ACTION: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                   CallbackQueryHandler(self.eco_products, pattern="^ecological_products$"),
                                                                   CallbackQueryHandler(self.ask_language, pattern="^сhange_language$"),
                                                                   CallbackQueryHandler(self.show_heat, pattern="^heat$"),
                                                                   decorative_technics_menu.handler,
                                                                   colored_solutions_menu.handler,
                                                                   health_menu.handler,
                                                                   contacts_menu.handler,
                                                                   CallbackQueryHandler(self.back_to_menu, pattern="^back$"),
                                                                   MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                              self.States.LANGUAGE: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                     MessageHandler(Filters.text, self.set_language),
                                                                     MessageHandler(Filters.all, to_state(self.States.LANGUAGE))],
                                              self.States.NAME: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                 MessageHandler(Filters.text, self.set_name),
                                                                 MessageHandler(Filters.all, to_state(self.States.NAME))],
                                              self.States.CONFIRM: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                    CallbackQueryHandler(self.entry, pattern="^back$"),
                                                                    CallbackQueryHandler(self.entry, pattern="^no$"),
                                                                    CallbackQueryHandler(self.confirm_name, pattern="^yes$"),
                                                                    MessageHandler(Filters.all, to_state(self.States.CONFIRM))],
                                              self.States.ECO: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                                CallbackQueryHandler(self.eco_products, pattern="^back_to_eco$"),
                                                                CallbackQueryHandler(self.paints_types, pattern="^paints$"),
                                                                CallbackQueryHandler(self.heat_systems, pattern="^in_fa$"),
                                                                heat_systems_menu.handler,
                                                                paints_menu.handler,
                                                                putty_and_plaster_menu.handler,
                                                                first_coats_menu.handler,
                                                                self.decorative_coats_menu.handler,
                                                                MessageHandler(Filters.all, to_state(self.States.ECO))],
                                              self.States.HEAT: [PrefixHandler('/', 'admin', self.goto_admin),
                                                                 CallbackQueryHandler(self.right_state(heat_systems_menu.entry), pattern="^to_product$"),
                                                                 CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                                 CallbackQueryHandler(self.show_heat, pattern="^back_to_heat$"),
                                                                 CallbackQueryHandler(self.show_heat_info, pattern="^heat_\w+$"),
                                                                 CallbackQueryHandler(self.show_heat_info, pattern="^menu$"),
                                                                 CallbackQueryHandler(self.show_scheme, pattern="^scheme$"),
                                                                 MessageHandler(Filters.all, to_state(self.States.HEAT))],
                                              self.States.PRODUCT: [CallbackQueryHandler(self.right_state(heat_systems_menu.show_picture), pattern='^picture$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.send_doc), pattern='^ti$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.show_features), pattern='^features$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.next_object), pattern=f'^next_object_{heat_systems_menu.menu_name}$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.prev_object), pattern=f'^prev_object_{heat_systems_menu.menu_name}$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.show_characteristics), pattern='^characteristics$'),
                                                                    CallbackQueryHandler(self.right_state(heat_systems_menu.entry), pattern='^menu$'),
                                                                    CallbackQueryHandler(self.back_to_menu, pattern='^back_to_list$'),
                                                                    CallbackQueryHandler(self.show_heat_info, pattern=f'^back_{heat_systems_menu.menu_name}$'),
                                                                    MessageHandler(Filters.all, to_state(self.States.PRODUCT))]
                                              },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
