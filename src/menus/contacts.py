import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import unknown_command, to_state, get_settings, generate_regex_handlers
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.settings import SETTINGS_FILE


class ContactsMenu(BaseMenu):
    menu_name = 'contacts_menu'

    class States(enum.Enum):
        ACTION = 1
        MAIN_OFFICE = 2
        NEAREST_CENTER = 3
        PARTNERS = 4

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Main office"), callback_data='main_office')],
                   [InlineKeyboardButton(_("Nearest Caparol center"), callback_data='nearest_center')],
                   [InlineKeyboardButton(_("Partners"), callback_data='partners')],
                   [InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def main_office(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["contacts"][language_code]["main_office"], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="MARKDOWNV2")

        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def nearest_center(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select region, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Central region"), callback_data='central')],
                   [InlineKeyboardButton(_("West region"), callback_data='west')],
                   [InlineKeyboardButton(_("East region"), callback_data='east')],
                   [InlineKeyboardButton(_("South region"), callback_data='south')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.NEAREST_CENTER

    def central(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "central"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Kiev"), callback_data='city_kiev')],
                   [InlineKeyboardButton(_("Cherkasy"), callback_data='city_cherkasy')],
                   [InlineKeyboardButton(_("Chernigov"), callback_data='city_chernigov')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.NEAREST_CENTER

    def west(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "west"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Vinnitsa"), callback_data='city_vinnitsa'),
                    InlineKeyboardButton(_("Lutsk"), callback_data='city_lutsk')],
                   [InlineKeyboardButton(_("Lviv"), callback_data='city_lviv'),
                    InlineKeyboardButton(_("Rivne"), callback_data='city_rivne')],
                   [InlineKeyboardButton(_("Ternopil"), callback_data='city_ternopil'),
                    InlineKeyboardButton(_("Uzhgorod"), callback_data='city_uzhgorod')],
                   [InlineKeyboardButton(_("Khmelnitsky"), callback_data='city_khmelnitsky')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.NEAREST_CENTER

    def east(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "east"

        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Dnipro"), callback_data='city_dnipro')],
                   [InlineKeyboardButton(_("Kropyvnytskyi"), callback_data='city_kropyvnytskyi')],
                   [InlineKeyboardButton(_("Kharkov"), callback_data='city_kharkov')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.NEAREST_CENTER

    def south(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "south"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Nikolaev"), callback_data='city_nikolaev')],
                   [InlineKeyboardButton(_("Odessa"), callback_data='city_odessa')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.NEAREST_CENTER

    def partners_nearest_center(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select region, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Central region"), callback_data='p_central')],
                   [InlineKeyboardButton(_("West region"), callback_data='p_west')],
                   [InlineKeyboardButton(_("East region"), callback_data='p_east')],
                   [InlineKeyboardButton(_("South region"), callback_data='p_south')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.PARTNERS

    def partners_central(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "p_central"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Kiev"), callback_data='p_city_kiev')],
                   [InlineKeyboardButton(_("Zhytomyr"), callback_data='p_city_zhytomyr')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.PARTNERS

    def partners_west(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "p_west"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Vinnitsa"), callback_data='p_city_vinnitsa'), InlineKeyboardButton(_("Ternopil"), callback_data='p_city_ternopil')],
                   [InlineKeyboardButton(_("Ivano-Frankivsk"), callback_data='p_city_ivano'), InlineKeyboardButton(_("Uzhgorod"), callback_data='p_city_uzhgorod')],
                   [InlineKeyboardButton(_("Lutsk"), callback_data='p_city_lutsk'), InlineKeyboardButton(_("Khmelnitsky"), callback_data='p_city_khmelnitsky')],
                   [InlineKeyboardButton(_("Lviv"), callback_data='p_city_lviv'), InlineKeyboardButton(_("Chernivtsi"), callback_data='p_city_chernivtsi')],
                   [InlineKeyboardButton(_("Rivne"), callback_data='p_city_rivne'), InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.PARTNERS

    def partners_east(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "p_east"

        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Dnipro"), callback_data='p_city_dnipro'), InlineKeyboardButton(_("Kremenchug"), callback_data='p_city_kremenchug')],
                   [InlineKeyboardButton(_("Zaporizhzhia"), callback_data='p_city_zaporizhzhia'), InlineKeyboardButton(_("Krivoy Rog"), callback_data='p_city_krivoy_rog')],
                   [InlineKeyboardButton(_("Kharkov"), callback_data='p_city_kharkov'), InlineKeyboardButton(_("Mariupol"), callback_data='p_city_mariupol')],
                   [InlineKeyboardButton(_("Poltava"), callback_data='p_city_poltava'), InlineKeyboardButton(_("Sumy"), callback_data='p_city_sumy')],
                   [InlineKeyboardButton(_("Kramatorsk"), callback_data='p_city_kramatorsk'), InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.PARTNERS

    def partners_south(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        context.user_data[self.menu_name]["region"] = "p_south"
        message_text = _("Select city, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Kherson"), callback_data='p_city_kherson')],
                   [InlineKeyboardButton(_("Odessa"), callback_data='p_city_odessa')],
                   [InlineKeyboardButton(_("Ismail"), callback_data='p_city_ismail')],
                   [InlineKeyboardButton(_("Uman"), callback_data='p_city_uman')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_nearest')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.PARTNERS

    def show_info(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("Back"), callback_data=context.user_data[self.menu_name]["region"])]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["contacts"][language_code][update.callback_query.data.replace("city_", "")], reply_markup=InlineKeyboardMarkup(buttons), parse_mode="MARKDOWNV2")
        return self.States.NEAREST_CENTER

    def partners_show_info(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("Back"), callback_data=context.user_data[self.menu_name]["region"])]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["partners"][language_code][update.callback_query.data.replace("p_city_", "")], reply_markup=InlineKeyboardMarkup(buttons), parse_mode="MARKDOWNV2")
        return self.States.PARTNERS

    def back(self, update, context):
        self.parent.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^contacts$")],
                                      states={self.States.ACTION: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                  [CallbackQueryHandler(self.back, pattern="^back$"),
                                                                   CallbackQueryHandler(self.entry, pattern="^back_to_menu$"),
                                                                   CallbackQueryHandler(self.main_office, pattern="^main_office$"),
                                                                   CallbackQueryHandler(self.nearest_center, pattern="^nearest_center$"),
                                                                   CallbackQueryHandler(self.partners_nearest_center, pattern="^partners$"),
                                                                   MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                              self.States.NEAREST_CENTER: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                          [CallbackQueryHandler(self.entry, pattern="^back_to_menu$"),
                                                                           CallbackQueryHandler(self.nearest_center, pattern="^back_to_nearest$"),
                                                                           CallbackQueryHandler(self.central, pattern="^central$"),
                                                                           CallbackQueryHandler(self.west, pattern="^west$"),
                                                                           CallbackQueryHandler(self.east, pattern="^east$"),
                                                                           CallbackQueryHandler(self.south, pattern="^south$"),
                                                                           CallbackQueryHandler(self.show_info, pattern=r"^city_\w+$"),
                                                                           MessageHandler(Filters.all, to_state(self.States.NEAREST_CENTER))],
                                              self.States.PARTNERS: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                    [CallbackQueryHandler(self.entry, pattern="^back_to_menu$"),
                                                                     CallbackQueryHandler(self.partners_nearest_center, pattern="^back_to_nearest$"),
                                                                     CallbackQueryHandler(self.partners_central, pattern="^p_central$"),
                                                                     CallbackQueryHandler(self.partners_west, pattern="^p_west$"),
                                                                     CallbackQueryHandler(self.partners_east, pattern="^p_east$"),
                                                                     CallbackQueryHandler(self.partners_south, pattern="^p_south$"),
                                                                     CallbackQueryHandler(self.partners_show_info, pattern=r"^p_city_\w+$"),
                                                                     MessageHandler(Filters.all, to_state(self.States.PARTNERS))],
                                              },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)
        return handler
