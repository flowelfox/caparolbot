import enum

from botmanlib.menus.helpers import add_to_db
from botmanlib.menus.list_menus.one_list_menu import OneListMenu
from botmanlib.messages import send_or_edit, delete_interface
from sqlalchemy import or_
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, ProductHeatSystems, ProductPaints, ProductFirstCoats, ProductPuttyAndPlaster


class HeatSystemsMenu(OneListMenu):
    menu_name = 'heat_systems_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        if update.callback_query.data == "heat_carbon":
            context.user_data["heat_type"] = "Capatect CARBON"
        elif update.callback_query.data == "heat_longlife":
            context.user_data["heat_type"] = "Capatect LONGLIFE"
        elif update.callback_query.data == "heat_classic":
            context.user_data["heat_type"] = "Capatect CLASSIC"
        elif update.callback_query.data == "heat_standart":
            context.user_data["heat_type"] = "Capatect STANDARD"
        self.update_objects(context)
        self._load(context)

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r'^heat_\w+$')]

    def back(self, update, context):
        self.parent.heat_systems(update, context)
        return self.parent.States.ECO

    def query_objects(self, context):

        user = context.user_data['user']
        i = []
        i.extend(list(DBSession.query(ProductHeatSystems) \
                      .filter(or_(ProductHeatSystems.additional_category.like(f'%{context.user_data["heat_type"]}%'), ProductHeatSystems.category == context.user_data["heat_type"])) \
                      .filter(ProductHeatSystems.language_code == user.language_code) \
                      .filter(ProductHeatSystems.hide == "true") \
                      .order_by(ProductHeatSystems.name).all()))
        i.extend(list(DBSession.query(ProductPuttyAndPlaster) \
                      .filter(ProductPuttyAndPlaster.additional_category.like(f'%{context.user_data["heat_type"]}%')) \
                      .filter(ProductPuttyAndPlaster.language_code == user.language_code) \
                      .filter(ProductPuttyAndPlaster.hide == "true") \
                      .order_by(ProductPuttyAndPlaster.name).all()))
        i.extend(list(DBSession.query(ProductPaints) \
                      .filter(ProductPaints.additional_category.like(f'%{context.user_data["heat_type"]}%')) \
                      .filter(ProductPaints.language_code == user.language_code) \
                      .filter(ProductPaints.hide == "true") \
                      .order_by(ProductPaints.name).all()))
        i.extend(list(DBSession.query(ProductFirstCoats) \
                      .filter(ProductFirstCoats.additional_category.like(f'%{context.user_data["heat_type"]}%')) \
                      .filter(ProductFirstCoats.language_code == user.language_code) \
                      .filter(ProductFirstCoats.hide == "true") \
                      .order_by(ProductFirstCoats.name).all()))
        return i

    def message_text(self, context, obj):
        _ = context.user_data['_']

        if obj:
            message_text = "<b>" + obj.name + "</b>\n"
            if obj.description is not None:
                message_text += obj.description + "\n\n"
        else:
            message_text = _("There is nothing to list\n")

        return message_text

    def object_buttons(self, context, obj):
        _ = context.user_data['_']

        buttons = []

        if obj.product_photo:
            buttons.append([InlineKeyboardButton(_("Show picture"), callback_data='picture')])
        if obj.ti:
            buttons.append([InlineKeyboardButton(_("Technical information"), callback_data='ti')])
        if obj.density or obj.color_shade or obj.gloss or obj.packing or obj.consumption:
            buttons.append([InlineKeyboardButton(_("Features"), callback_data="features")])
        if obj.vlastivosti:
            buttons.append([InlineKeyboardButton(_("Characteristics"), callback_data="characteristics")])

        return buttons

    def show_features(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        obj = context.user_data[self.menu_name]['objects'][context.user_data[self.menu_name]['selected_object']]

        message_text = ""
        if obj.vlastivosti is not None:
            message_text += obj.vlastivosti + "\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]
        if not message_text:
            message_text = _("There is nothing to show\n")
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ACTION

    def show_characteristics(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        obj = context.user_data[self.menu_name]['objects'][context.user_data[self.menu_name]['selected_object']]
        message_text = ""
        if obj.density is not None:
            message_text += "<b>" + _("Density") + ":</b> "
            message_text += obj.density + "\n"
        if obj.consumption is not None:
            message_text += "<b>" + _("Consumption") + ":</b> "
            message_text += obj.consumption + "\n"
        if obj.color_shade is not None:
            message_text += "<b>" + _("Color") + ":</b> "
            message_text += obj.color_shade + "\n"
        if obj.gloss is not None:
            message_text += "<b>" + _("Gloss") + ":</b> "
            message_text += obj.gloss + "\n"
        if obj.packing is not None:
            message_text += "<b>" + _("Packing") + ":</b> "
            message_text += obj.packing + "\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]
        if not message_text:
            message_text = _("There is nothing to show\n")
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def show_picture(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]
        selected_object = self.selected_object(context)
        interface = send_or_edit(context, chat_id=user.chat_id, photo=selected_object.product_photo, reply_markup=InlineKeyboardMarkup(buttons))
        photo_id = interface.photo[-1].file_id

        if 'http' in selected_object.product_photo:
            selected_object.product_photo = photo_id
            if not add_to_db(selected_object):
                return self.conv_fallback(context)

        return self.States.ACTION

    def send_doc(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]
        selected_object = self.selected_object(context)
        interface = send_or_edit(context, chat_id=user.chat_id, document=selected_object.ti, reply_markup=InlineKeyboardMarkup(buttons))

        file_id = interface.document.file_id

        if 'http' in selected_object.ti:
            selected_object.ti = file_id
            if not add_to_db(selected_object):
                return self.conv_fallback(context)

        return self.States.ACTION

    def back_to_list(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):

        return {self.States.ACTION: [CallbackQueryHandler(self.show_picture, pattern='^picture$'),
                                     CallbackQueryHandler(self.show_features, pattern='^features$'),
                                     CallbackQueryHandler(self.show_characteristics, pattern='^characteristics$'),
                                     CallbackQueryHandler(self.send_doc, pattern='^ti$'),
                                     CallbackQueryHandler(self.back_to_list, pattern='^back_to_list$')]}
