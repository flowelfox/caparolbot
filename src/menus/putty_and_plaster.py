import enum

from botmanlib.menus.helpers import group_buttons, add_to_db
from botmanlib.menus.list_menus.bunch_list_menu import BunchListMenu
from botmanlib.messages import send_or_edit, delete_interface
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from src.models import DBSession, ProductPuttyAndPlaster


class PuttyAndPlasterMenu(BunchListMenu):
    menu_name = 'putty_and_plaster_menu'
    objects_per_page = 12
    auto_hide_arrows = True
    send_as_markdown = True
    search_key_param = "name"
    model = ProductPuttyAndPlaster

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        if update.callback_query.data == "typep_putty":
            context.user_data["p_and_p_type"] = "Шпаклівки"
        elif update.callback_query.data == "typep_plaster":
            context.user_data["p_and_p_type"] = "Штукатурки"
        self.update_objects(context)
        self._load(context)

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^typep_\w+$')]

    def back(self, update, context):
        self.parent.eco_products(update, context)
        return self.parent.States.ECO

    def query_objects(self, context):
        user = context.user_data['user']
        return DBSession.query(ProductPuttyAndPlaster) \
            .filter(ProductPuttyAndPlaster.category == context.user_data["p_and_p_type"]) \
            .filter(ProductPuttyAndPlaster.language_code == user.language_code) \
            .filter(ProductPuttyAndPlaster.hide == "true") \
            .order_by(ProductPuttyAndPlaster.name).all()

    def page_text(self, current_page, max_page, user_data):
        return ""

    def message_text(self, context, page_objects):
        user = context.user_data['user']
        _ = context.user_data['_']

        if page_objects:
            message_text = _("Select an action, {name}.\n\n").format(name=user.desired_name)
            message_text += _("For searching write first letter in Latin.")
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def object_buttons(self, context, page_objects):
        _ = context.user_data['_']

        if page_objects:
            buttons_flat = []
            for obj in page_objects:
                buttons_flat.append(InlineKeyboardButton(obj.name, callback_data=f'id{obj.id}'))
            buttons = group_buttons(buttons_flat, 2)
        else:
            buttons = []

        return buttons

    def show_product(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if "id" in update.callback_query.data:
            obj = DBSession.query(ProductPuttyAndPlaster).filter(ProductPuttyAndPlaster.id == int(update.callback_query.data.replace("id", ""))).first()
        else:
            obj = context.user_data[self.menu_name]["obj"]
        if obj:
            context.user_data[self.menu_name]["obj"] = obj
            message_text = "<b>" + obj.name + "</b>\n"
            if obj.description is not None:
                message_text += obj.description + "\n\n"

            buttons = []

            if obj.product_photo:
                buttons.append([InlineKeyboardButton(_("Show picture"), callback_data='picture')])
            if obj.ti:
                buttons.append([InlineKeyboardButton(_("Technical information"), callback_data=f'ti')])
            if obj.density or obj.color_shade or obj.gloss or obj.packing or obj.consumption or obj.grain or obj.structure:
                buttons.append([InlineKeyboardButton(_("Features"), callback_data=f"features_{obj.id}")])
            if obj.vlastivosti:
                buttons.append([InlineKeyboardButton(_("Characteristics"), callback_data=f"characteristics_{obj.id}")])

            buttons.append([InlineKeyboardButton(_("Back"), callback_data=f'back_to_list')])

            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
            return self.States.ACTION

        else:
            buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]
            message_text = _("There is nothing to list\n")
            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
            return self.States.ACTION

    def show_features(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        data = int(update.callback_query.data.replace("features_", ""))
        obj = DBSession.query(ProductPuttyAndPlaster).get(data)

        message_text = ""
        if obj.vlastivosti is not None:
            message_text += obj.vlastivosti + "\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="menu")]]
        if not message_text:
            message_text = _("There is nothing to show\n")
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ACTION

    def show_characteristics(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        data = int(update.callback_query.data.replace("characteristics_", ""))
        obj = DBSession.query(ProductPuttyAndPlaster).get(data)

        message_text = ""
        if obj.density is not None:
            message_text += "<b>" + _("Density") + ":</b> "
            message_text += obj.density + "\n"
        if obj.consumption is not None:
            message_text += "<b>" + _("Consumption") + ":</b> "
            message_text += obj.consumption + "\n"
        if obj.color_shade is not None:
            message_text += "<b>" + _("Color") + ":</b> "
            message_text += obj.color_shade + "\n"
        if obj.gloss is not None:
            message_text += "<b>" + _("Gloss") + ":</b> "
            message_text += obj.gloss + "\n"
        if obj.packing is not None:
            message_text += "<b>" + _("Packing") + ":</b> "
            message_text += obj.packing + "\n"
        if obj.grain is not None:
            message_text += "<b>" + _("Grain") + ":</b> "
            message_text += obj.grain + "\n"
        if obj.structure is not None:
            message_text += "<b>" + _("Structure") + ":</b> "
            message_text += obj.structure + "\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="menu")]]
        if not message_text:
            message_text = _("There is nothing to show\n")
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def show_picture(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="menu")]]

        interface = send_or_edit(context, chat_id=user.chat_id, photo=context.user_data[self.menu_name]["obj"].product_photo, reply_markup=InlineKeyboardMarkup(buttons))
        photo_id = interface.photo[-1].file_id

        if 'http' in context.user_data[self.menu_name]["obj"].product_photo:
            context.user_data[self.menu_name]["obj"].product_photo = photo_id
            if not add_to_db(context.user_data[self.menu_name]["obj"]):
                return self.conv_fallback(context)

        return self.States.ACTION

    def send_doc(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="menu")]]

        interface = send_or_edit(context, chat_id=user.chat_id, document=context.user_data[self.menu_name]["obj"].ti, reply_markup=InlineKeyboardMarkup(buttons))

        file_id = interface.document.file_id

        if 'http' in context.user_data[self.menu_name]["obj"].ti:
            context.user_data[self.menu_name]["obj"].ti = file_id
            if not add_to_db(context.user_data[self.menu_name]["obj"]):
                return self.conv_fallback(context)

        return self.States.ACTION

    def back_to_list(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):

        return {self.States.ACTION: [CallbackQueryHandler(self.show_product, pattern='^id\d+$'),
                                     CallbackQueryHandler(self.show_picture, pattern='^picture$'),
                                     CallbackQueryHandler(self.send_doc, pattern='^ti$'),
                                     CallbackQueryHandler(self.show_features, pattern='^features_\d+$'),
                                     CallbackQueryHandler(self.show_characteristics, pattern='^characteristics_\d+$'),
                                     CallbackQueryHandler(self.show_product, pattern='^menu$'),
                                     CallbackQueryHandler(self.back_to_list, pattern='^back_to_list$')]}
