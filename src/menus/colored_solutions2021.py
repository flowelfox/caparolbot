import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import unknown_command, to_state, get_settings, write_settings, generate_regex_handlers
from botmanlib.messages import delete_interface, send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.el_versions import ElectronicVersionsMenu
from src.settings import SETTINGS_FILE


class ColoredSolutionsMenu(BaseMenu):
    menu_name = 'colored_solutions_menu'

    class States(enum.Enum):
        ACTION = 1
        SAFE = 2
        EL_VER = 3
        VERSIONS = 4
        PALETTE = 5

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [
                   [InlineKeyboardButton(_("Caparol Color Express"), callback_data='cce')],
                   [InlineKeyboardButton(_("Trends 2021"), callback_data='trend_colors')],
                   [InlineKeyboardButton(_("Electronic versions of coloring"), callback_data='el_versions')],
                   [InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def cce(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["colored_solutions"][language_code]["cce"], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")

        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def trend_colors(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)
        buttons = [
            # [InlineKeyboardButton(_("Trend object"), callback_data='trend_object')],
                   [InlineKeyboardButton(_("Color 2021"), callback_data='color_2021')],
                   [InlineKeyboardButton(_("Palette 1"), callback_data='pallet_1')],
                   [InlineKeyboardButton(_("Palette 2"), callback_data='pallet_2')],
                   [InlineKeyboardButton(_("Palette 3"), callback_data='pallet_3')],
                   [InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["colored_solutions"][language_code]["trend_colors"], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")

        return self.States.PALETTE

    def trend_object(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="trend_colors")]]
        settings = get_settings(SETTINGS_FILE)

        message_text = f"<a href=\"{settings['colored_solutions'][language_code]['trend_object_img']}\">\u200B</a>"
        message_text += settings["colored_solutions"][language_code]['trend_object']
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")

        return self.States.PALETTE

    def color_2021(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"

        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="trend_colors")]]
        settings = get_settings(SETTINGS_FILE)

        message_text = f"<a href=\"{settings['colored_solutions'][language_code]['color_2021_img']}\">\u200B</a>"
        message_text += settings["colored_solutions"][language_code]['color_2021']
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")

        return self.States.PALETTE

    def palette(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"

        data = update.callback_query.data
        delete_interface(context)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="trend_colors")]]
        settings = get_settings(SETTINGS_FILE)

        interface = send_or_edit(context, chat_id=user.chat_id, photo=settings["pallets"][data], caption=settings['colored_solutions'][language_code][data], reply_markup=InlineKeyboardMarkup(buttons))

        if 'http' in settings["pallets"][data]:
            settings["pallets"][data] = interface.photo[-1].file_id
            write_settings({"pallets": settings["pallets"]}, SETTINGS_FILE)

        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def back_to_menu(self, update, context):
        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def get_handler(self):
        el_versions_menu = ElectronicVersionsMenu(self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^colored_solutions")],
                                      states={self.States.ACTION: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                  [CallbackQueryHandler(self.back, pattern="^back$"),
                                                                   CallbackQueryHandler(self.back_to_menu, pattern=r"^back_to_menu$"),
                                                                   CallbackQueryHandler(self.trend_colors, pattern="^trend_colors$"),
                                                                   CallbackQueryHandler(self.cce, pattern="^cce$"),
                                                                   el_versions_menu.handler,

                                                                   MessageHandler(Filters.all, to_state(self.States.ACTION))],
                                              self.States.EL_VER: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                  [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                                   el_versions_menu.handler,

                                                                   MessageHandler(Filters.all, to_state(self.States.EL_VER))],
                                              self.States.PALETTE: generate_regex_handlers("Main menu", self.parent.entry) +
                                                                   [CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                                    CallbackQueryHandler(self.palette, pattern=r"^pallet_\d+$"),
                                                                    CallbackQueryHandler(self.color_2021, pattern=r"^color_2021$"),
                                                                    CallbackQueryHandler(self.trend_object, pattern=r"^trend_object$"),
                                                                    CallbackQueryHandler(self.trend_colors, pattern="^trend_colors$"),
                                                                    MessageHandler(Filters.all, to_state(self.States.PALETTE))],
                                              },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)
        return handler
