import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import unknown_command, to_state, get_settings, write_settings
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.settings import SETTINGS_FILE


class ElectronicVersionsMenu(BaseMenu):
    menu_name = 'electronic_versions_menu'

    class States(enum.Enum):
        ACTION = 1
        DOWNLOAD = 2

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)
        buttons = [[InlineKeyboardButton(_("RGB"), callback_data='rgb')],
                   [InlineKeyboardButton(_("CMYK"), callback_data='cmyk')],
                   [InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=settings["colored_solutions"][language_code]["el_versions"], reply_markup=InlineKeyboardMarkup(buttons))

    def rgb(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)
        buttons = [[InlineKeyboardButton(_("ASE - Format"), callback_data='rgb_ase'),
                    InlineKeyboardButton(_("JPG - Format"), callback_data='rgb_jpg')],
                   [InlineKeyboardButton(_("TIF - Format"), callback_data='rgb_tif'),
                    InlineKeyboardButton(_("XLSX - Format"), callback_data='rgb_xlsx')],
                   [InlineKeyboardButton(_("PDF - Format"), callback_data='rgb_pdf'),
                    InlineKeyboardButton(_("PAL - Format"), callback_data='rgb_pal')],
                   [InlineKeyboardButton(_("GPL - Format"), callback_data='rgb_gpl'),
                    InlineKeyboardButton(_("SKM - Format"), callback_data='rgb_skm')],
                   [InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["colored_solutions"][language_code]["rgb"], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True))

        return self.States.DOWNLOAD

    def cmyk(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)
        buttons = [[InlineKeyboardButton(_("ASE - Format"), callback_data='cmyk_ase')],
                   [InlineKeyboardButton(_("JPG - Format"), callback_data='cmyk_jpg')],
                   [InlineKeyboardButton(_("TIF - Format"), callback_data='cmyk_tif')],
                   [InlineKeyboardButton(_("XLSX - Format"), callback_data='cmyk_xlsx')],
                   [InlineKeyboardButton(_("PDF - Format"), callback_data='cmyk_pdf')],
                   [InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["colored_solutions"][language_code]["cmyk"], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True))

        return self.States.DOWNLOAD

    def download(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        data = update.callback_query.data.split("_")
        settings = get_settings(SETTINGS_FILE)
        if data[0] == "rgb":
            buttons = [[InlineKeyboardButton(_("Back"), callback_data="rgb")]]
        else:
            buttons = [[InlineKeyboardButton(_("Back"), callback_data="cmyk")]]

        interface = send_or_edit(context, chat_id=user.chat_id, document=settings["colors"][data[0]][data[1]], reply_markup=InlineKeyboardMarkup(buttons))
        settings["colors"][data[0]][data[1]] = interface.document.file_id
        write_settings({"colors": settings["colors"]}, SETTINGS_FILE)
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        update.callback_query.answer()
        return ConversationHandler.END

    def back_to_menu(self, update, context):
        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def get_handler(self):

        handler = ConversationHandler(
            entry_points=[CallbackQueryHandler(self.entry, pattern="^el_versions")],
            states={self.States.ACTION: [CallbackQueryHandler(self.back, pattern="^back$"),
                                         CallbackQueryHandler(self.rgb, pattern="^rgb$"),
                                         CallbackQueryHandler(self.cmyk, pattern="^cmyk$"),
                                         MessageHandler(Filters.all, to_state(self.States.ACTION))],
                    self.States.DOWNLOAD: [
                        CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                        CallbackQueryHandler(self.download, pattern=r"^(cmyk|rgb)_\w+$"),

                        MessageHandler(Filters.all, to_state(self.States.DOWNLOAD))],
                    },
            fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
            allow_reentry=True)
        return handler
