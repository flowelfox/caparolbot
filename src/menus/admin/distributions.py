import enum

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, group_buttons, add_to_db
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from botmanlib.models import Database
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import Unauthorized, BadRequest
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram.utils.promise import Promise

from src.menus.admin.polls import PollDistributionMenu
from src.models import User, Distribution, Reaction, DBSession, JobSession, DistributionMessage


class DistributionsMenu(BaseMenu):
    menu_name = 'distribution_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Instant distribution"), callback_data='instant_distribution')],
                   [InlineKeyboardButton(_("Poll distribution"), callback_data='poll_distribution')],
                   [InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Choose distribution type:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        instant_distribution_menu = InstantDistributionMenu(User, self)
        poll_distribution_menu = PollDistributionMenu(self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^distribution$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               instant_distribution_menu.handler,
                                                               poll_distribution_menu.handler,
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class InstantDistributionMenu(BaseMenu):
    menu_name = "instant_distribution_menu"

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        REACTIONS = 6
        ADD_REACTION = 7

    def __init__(self, user_model, parent=None, bot=None, dispatcher=None):
        self.user_model = user_model
        super(InstantDistributionMenu, self).__init__(parent, bot, dispatcher)

    def entry(self, update, context):
        _ = context.user_data['user'].translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []
        context.user_data[self.menu_name]['reactions'] = []

        self.send_message(context)
        return self.States.ACTION

    def available_buttons(self, context):
        _ = context.user_data['user'].translator
        return [{"data": "start", "name": _("Return user to start menu")},
                {"data": "close_message", "name": _("Close distribution message. Do not use if you have more than one message!")}]

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("Messages: ") + "\n"

        buttons = []
        line = []
        line.append(InlineKeyboardButton("🔙", callback_data=f"back_{self.menu_name}"))
        if len(context.user_data[self.menu_name]['messages']) <= 5:
            line.append(InlineKeyboardButton("🆕", callback_data='new_message'))
        line.append(InlineKeyboardButton("✅", callback_data='start_distribution'))
        if self.available_buttons(context):
            line.append(InlineKeyboardButton("🔗", callback_data='add_button'))

        buttons.append(line)
        buttons.append([InlineKeyboardButton(_("Reactions"), callback_data="reactions")])

        messages = context.user_data[self.menu_name]['messages']
        if not messages:
            message_text += _("Add new message using button below") + "\n"
        else:
            for idx, message in enumerate(messages):
                message_text += "{}. ".format(idx + 1)
                if message['text']:
                    message_text += _("Text") + "\n"
                    buttons.append([InlineKeyboardButton("{}. Text".format(idx + 1), callback_data=f"message_{idx}")])
                elif message['photo_id']:
                    message_text += _("Photo") + "\n"
                    buttons.append([InlineKeyboardButton("{}. Photo".format(idx + 1), callback_data=f"message_{idx}")])

        if context.user_data[self.menu_name]['buttons']:
            message_text += "\n"
            message_text += _("Buttons:") + " " + ", ".join([item[1] for item in context.user_data[self.menu_name]['buttons']])

        if context.user_data[self.menu_name]['reactions']:
            message_text += "\n"
            message_text += _("Reactions:") + "\n"
            for idx, reaction in enumerate(context.user_data[self.menu_name]['reactions']):
                message_text += f"{idx + 1}. {reaction}\n"

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def reactions(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""

        if context.user_data[self.menu_name]['reactions']:
            for idx, reaction in enumerate(context.user_data[self.menu_name]['reactions']):
                message_text += f"{idx + 1}. {reaction}\n"
        else:
            message_text += _("You didn't add any reactions yet")

        line = []
        if len(context.user_data[self.menu_name]['reactions']) < 6:
            line.append(InlineKeyboardButton(_("Add"), callback_data="add_reaction"))
        line.append(InlineKeyboardButton(_("Remove"), callback_data="remove_reaction"))
        buttons = [line, [InlineKeyboardButton(_("Back"), callback_data="back_to_messages")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.REACTIONS

    def add_reaction(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("Please enter text or emoji that would be displayed on button:")
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_reactions")]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ADD_REACTION

    def save_reaction(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        text = update.effective_message.text
        context.user_data[self.menu_name]['reactions'].append(text)
        delete_interface(context)
        return self.reactions(update, context)

    def remove_reaction(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        message_text = _("Please select reaction to remove:")

        btns = []
        for idx, reaction in enumerate(context.user_data[self.menu_name]['reactions']):
            btns.append(InlineKeyboardButton(f"{idx + 1}. {reaction}", callback_data=f"remove_reaction_{idx}"))

        buttons.extend(group_buttons(btns))
        buttons.append([InlineKeyboardButton(_("Back"), callback_data="back_to_reactions")])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.REACTIONS

    def remove_selected_reaction(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        reaction_id = int(update.callback_query.data.replace("remove_reaction_", ""))

        update.callback_query.answer(_("Reaction {reaction} removed").format(reaction=context.user_data[self.menu_name]['reactions'][reaction_id]), show_alert=True)

        del context.user_data[self.menu_name]['reactions'][reaction_id]
        return self.remove_reaction(update, context)

    def back_to_messages(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def back_to_message(self, update, context):
        update.callback_query.date = f"message_{context.user_data[self.menu_name]['selected_message']}"
        context.update_queue.put(update)

        return self.States.VIEW_MESSAGE

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def ask_new_message(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_to_messages')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Send me text message or photo with or without description."), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.NEW_MESSAGE

    def new_message(self, update, context):
        _ = context.user_data['user'].translator

        if update.message.photo:
            photo_id = update.message.photo[-1].file_id
            caption = update.message.caption
            mes = {'photo_id': photo_id,
                   'caption': caption,
                   'text': None}
            context.user_data[self.menu_name]['messages'].append(mes)

        elif update.message.text:
            mes = {'photo_id': None,
                   'caption': None,
                   'text': update.message.text}
            context.user_data[self.menu_name]['messages'].append(mes)

        delete_interface(context)

        self.send_message(context)
        return self.States.ACTION

    def view_message(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        try:
            data = int(update.callback_query.data.replace("message_", ""))
            context.user_data[self.menu_name]['selected_message'] = data
        except (ValueError, AttributeError):
            data = context.user_data[self.menu_name]['selected_message']

        buttons = [[InlineKeyboardButton(_("Delete"), callback_data=f'delete_message_{data}')],
                   [InlineKeyboardButton(_("Back"), callback_data='back_to_messages')]]

        message = context.user_data[self.menu_name]['messages'][data]
        if message['text']:
            send_or_edit(context, chat_id=user.chat_id, text=message['text'], reply_markup=InlineKeyboardMarkup(buttons))
        elif message['photo_id']:
            buttons.append([InlineKeyboardButton(_("Edit caption"), callback_data=f'edit_caption')])
            send_or_edit(context, chat_id=user.chat_id, photo=message['photo_id'], caption=message['caption'], reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.VIEW_MESSAGE

    def delete_message(self, update, context):
        data = int(update.callback_query.data.replace("delete_message_", ""))

        del context.user_data[self.menu_name]['messages'][data]
        self.send_message(context)
        return self.States.ACTION

    def edit_caption_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_to_message')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Send me new caption for photo."), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.EDIT_CAPTION

    def edit_caption(self, update, context):
        _ = context.user_data['user'].translator

        message = context.user_data[self.menu_name]['messages'][context.user_data[self.menu_name]['selected_message']]
        message.update(caption=update.message.text)

        return self.view_message(update, context)

    def ask_add_button(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        flat_buttons = []
        message_text = _("Please select callback that you want to add:") + '\n'
        for callback_data in self.available_buttons(context):
            message_text += callback_data['data'] + " - " + callback_data['name'] + '\n'
            if callback_data['data'] in [item[0] for item in context.user_data[self.menu_name]['buttons']]:
                flat_buttons.append(InlineKeyboardButton(f"➖ {callback_data['data'].capitalize()}", callback_data=f"remove_button_{callback_data['data']}"))
            else:
                flat_buttons.append(InlineKeyboardButton(f"➕ {callback_data['data'].capitalize()}", callback_data=f"add_button_{callback_data['data']}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.append([InlineKeyboardButton(_("Back"), callback_data='back_to_messages')])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ADD_BUTTON

    def ask_button_text(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['add_button_data'] = update.callback_query.data.replace("add_button_", "")
        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_to_buttons')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Please enter text which would be displayed on button"), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ADD_BUTTON

    def add_button(self, update, context):
        data = context.user_data[self.menu_name]['add_button_data']
        text = update.message.text
        context.user_data[self.menu_name]['buttons'].append((data, text))
        delete_interface(context)
        return self.ask_add_button(update, context)

    def remove_button(self, update, context):
        data = update.callback_query.data.replace("remove_button_", "")
        for button in context.user_data[self.menu_name]['buttons']:
            if data in button[0]:
                context.user_data[self.menu_name]['buttons'].remove(button)
        return self.ask_add_button(update, context)

    def ask_start_distribution(self, update, context):
        _ = context.user_data['user'].translator
        user = context.user_data['user']

        if not context.user_data[self.menu_name]['messages']:
            context.bot.answer_callback_query(update.callback_query.id, text=_("You must add at least one message."), show_alert=True)
            return self.States.ACTION

        buttons = [[InlineKeyboardButton(_("Yes"), callback_data="start_distribution_yes"),
                    InlineKeyboardButton(_("No"), callback_data="back_to_messages")]]
        send_or_edit(context, chat_id=user.chat_id, text=self.ask_start_distribution_text(context), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['user'].translator
        return _("Messages will be sent to all bot users. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['user'].translator
        return _("Message was sent to all bot users except you")

    def start_distribution(self, update, context):
        _ = context.user_data['user'].translator

        distribution = Distribution()
        for reaction_text in context.user_data[self.menu_name]['reactions']:
            reaction = Reaction(text=reaction_text, distribution=distribution)
            DBSession.add(reaction)

        DBSession.add(distribution)
        DBSession.commit()

        context.user_data[self.menu_name]['distribution_id'] = distribution.id

        job_context = {'user_data': context.user_data,
                       'update_queue': context.update_queue}
        context.job_queue.run_once(self.distribution_job, 0, context=job_context)

        context.bot.answer_callback_query(update.callback_query.id, text=self.after_start_distribution_text(context), show_alert=True)

        return self.back(update, context)

    def users_to_send(self, context):
        session = Database().sessionmaker()
        return session.query(self.user_model).filter(self.user_model.is_active == True).filter(self.user_model.chat_id != context.user_data["user"].chat_id).all()

    def distribution_job(self, context):
        context._user_data = context.job.context['user_data']
        messages = context.user_data[self.menu_name]['messages']
        buttons_data = context.user_data[self.menu_name]['buttons']
        reactions = context.user_data[self.menu_name]['reactions']
        distribution_id = context.user_data[self.menu_name]['distribution_id']
        distribution = JobSession.query(Distribution).get(distribution_id)

        users = self.users_to_send(context)

        for user in users:
            buttons = []
            if buttons_data:
                flat_buttons = []
                for data, name in buttons_data:
                    flat_buttons.append(InlineKeyboardButton(name, callback_data=data))
                buttons.extend(group_buttons(flat_buttons))
            if reactions:
                flat_buttons = []
                for idx, reaction in enumerate(distribution.reactions):
                    flat_buttons.append(InlineKeyboardButton(f"{reaction.text} - 0", callback_data=f"update_reaction_{reaction.id}"))
                buttons.extend(group_buttons(flat_buttons, 4))

            markup = InlineKeyboardMarkup(buttons) if buttons else None

            try:
                mes = None
                for message in messages:
                    if message['text']:
                        mes = context.bot.send_message(chat_id=user.chat_id, text=message['text'], disable_web_page_preview=True, reply_markup=markup if message is messages[-1] else None)
                    elif message['photo_id']:
                        mes = context.bot.send_photo(chat_id=user.chat_id, photo=message['photo_id'], caption=message['caption'], disable_web_page_preview=True, reply_markup=markup if message is messages[-1] else None)
                    else:
                        continue
                    self.logger.info(f"Distribution message was sent to user {user.chat_id}")
                if mes is not None:
                    if isinstance(mes, Promise):
                        mes.done.wait()
                        mes = mes.result()
                    JobSession.add(DistributionMessage(chat_id=user.chat_id, message_id=mes.message_id, distribution_id=distribution_id))

            except (Unauthorized, BadRequest):
                self.logger.info(f"Distribution message was NOT sent to user {user.chat_id}")
                continue

        if reactions:
            job_context = {"distribution_id": distribution_id}
            context.job_queue.run_once(self.update_reaction_job, 60, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 2, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 5, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 10, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 30, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 2, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 3, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 4, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 5, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 6, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 7, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 8, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 9, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 10, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 11, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 12, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 15, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 17, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 20, context=job_context)
            context.job_queue.run_once(self.update_reaction_job, 60 * 60 * 24, context=job_context)

    def update_reaction_job(self, context):
        distribution_id = context.job.context['distribution_id']
        distribution = JobSession.query(Distribution).get(distribution_id)

        for message in distribution.messages:
            flat_buttons = []
            for idx, reaction in enumerate(distribution.reactions):
                flat_buttons.append(InlineKeyboardButton(f"{reaction.text} - {len(reaction.users)}", callback_data=f"update_reaction_{reaction.id}"))

            buttons = group_buttons(flat_buttons, 4)
            markup = InlineKeyboardMarkup(buttons)

            try:
                context.bot.edit_message_reply_markup(chat_id=message.chat_id, message_id=message.message_id, reply_markup=markup)
            except BadRequest:
                pass
        self.logger.info(f"Updated reactions on distribution messages from {distribution.create_date.strftime('%d.%m.%Y %H:%M')}")


    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^instant_distribution$')]

    def additional_states(self):
        return {}

    def get_handler(self):

        states = {
            self.States.ACTION: [CallbackQueryHandler(self.view_message, pattern="message_\d+"),
                                 CallbackQueryHandler(self.ask_new_message, pattern="new_message"),
                                 CallbackQueryHandler(self.ask_add_button, pattern="add_button"),
                                 CallbackQueryHandler(self.start_distribution, pattern="start_distribution_yes"),
                                 CallbackQueryHandler(self.ask_start_distribution, pattern="start_distribution"),
                                 CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                 CallbackQueryHandler(self.back_to_messages, pattern='^back_to_messages$'),
                                 CallbackQueryHandler(self.reactions, pattern='^reactions$'),
                                 MessageHandler(Filters.all, to_state(self.States.ACTION))],

            self.States.NEW_MESSAGE: [MessageHandler(Filters.text | Filters.photo, self.new_message),
                                      CallbackQueryHandler(self.back_to_messages, pattern='^back_to_messages$'),
                                      MessageHandler(Filters.all, to_state(self.States.NEW_MESSAGE))],
            self.States.VIEW_MESSAGE: [CallbackQueryHandler(self.back_to_messages, pattern='^back_to_messages$'),
                                       CallbackQueryHandler(self.edit_caption_ask, pattern='edit_caption'),
                                       CallbackQueryHandler(self.delete_message, pattern='delete_message_\d+'),
                                       MessageHandler(Filters.all, to_state(self.States.VIEW_MESSAGE))],
            self.States.EDIT_CAPTION: [MessageHandler(Filters.text, self.edit_caption),
                                       CallbackQueryHandler(self.back_to_message, pattern='^back_to_message$'),
                                       MessageHandler(Filters.all, to_state(self.States.EDIT_CAPTION))],
            self.States.ADD_BUTTON: [CallbackQueryHandler(self.ask_button_text, pattern='^add_button_.+$'),
                                     MessageHandler(Filters.text, self.add_button),
                                     CallbackQueryHandler(self.remove_button, pattern='^remove_button_.+$'),
                                     CallbackQueryHandler(self.back_to_message, pattern='^back_to_messages$'),
                                     CallbackQueryHandler(self.ask_add_button, pattern='^back_to_buttons$'),
                                     MessageHandler(Filters.all, to_state(self.States.ADD_BUTTON))],
            self.States.REACTIONS: [CallbackQueryHandler(self.back_to_messages, pattern='^back_to_messages$'),
                                    CallbackQueryHandler(self.add_reaction, pattern='^add_reaction$'),
                                    CallbackQueryHandler(self.remove_reaction, pattern='^remove_reaction$'),
                                    CallbackQueryHandler(self.reactions, pattern='^back_to_reactions$'),
                                    CallbackQueryHandler(self.remove_selected_reaction, pattern='^remove_reaction_\d+$')
                                    ],

            self.States.ADD_REACTION: [CallbackQueryHandler(self.reactions, pattern='^back_to_reactions$'),
                                       MessageHandler(Filters.text, self.save_reaction)]
        }

        additional_states = self.additional_states()
        for key in additional_states:
            if key in states:
                states[key] = additional_states[key] + states[key]
            else:
                states.update({key: additional_states[key]})

        handler = ConversationHandler(entry_points=self.entry_points(),
                                      states=states,
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
