from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db
from botmanlib.messages import delete_interface
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from botmanlib.models import MessageType
from src.models import ProductDecorativeCoatings, DBSession


class DecorativeTypesAdminMenu(OneListMenu):
    menu_name = "products_decorative_types_admin_menu"
    search_key_param = 'name'
    model = ProductDecorativeCoatings
    send_as_markdown = True
    send_as_html = False

    def query_objects(self, user_data):
        return DBSession.query(ProductDecorativeCoatings).filter(ProductDecorativeCoatings.language_code != None).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^decorative_types$")]

    def message_text(self, context, obj):
        _ = context.user_data['_']

        if obj:
            bs = be = '**'
            message_text = ""
            if obj.name is not None:
                message_text += obj.name + "\n\n"
            if obj.description is not None:
                message_text += obj.description + "\n\n"
            if obj.vlastivosti is not None:
                message_text += obj.vlastivosti + "\n"
            if obj.density is not None:
                message_text += bs + _("Density") + ':' + be
                message_text += obj.density + "\n"
            if obj.consumption is not None:
                message_text += bs + _("Consumption") + ':' + be
                message_text += obj.consumption + "\n"
            if obj.color_shade is not None:
                message_text += bs + _("Color") + ':' + be
                message_text += obj.color_shade + "\n"
            if obj.gloss is not None:
                message_text += bs + _("Gloss") + ':' + be
                message_text += obj.gloss + "\n"
            if obj.packing is not None:
                message_text += bs + _("Packing") + ':' + be
                message_text += obj.packing + "\n"
            if obj.category is not None:
                message_text += bs + _("Category") + ':' + be
                message_text += obj.category + "\n"
            if obj.language_code is not None:
                message_text += bs + _("Language") + ':' + be + obj.language_code + "\n"
            if obj.hide is not None:
                message_text += bs + _("Visibility") + ':' + be + (_("Visible") if obj.hide is True else _("Invisible")) + "\n"

        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        _ = context.user_data['_']
        buttons = []
        buttons.append(InlineKeyboardButton(_("Add"), callback_data=f"add_decorative_type"))
        if obj:
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_decorative_type_{obj.id}"))
            buttons.append(InlineKeyboardButton(_("Hide") if obj.hide is True else _("Unhide"), callback_data=f"edit_visible"))

        return buttons

    def edit_visible(self, update, context):
        obj = context.user_data[self.menu_name]["objects"][context.user_data[self.menu_name]["selected_object"]]
        if obj.hide is True:
            obj.hide = False
        else:
            obj.hide = True
        DBSession.add(obj)
        DBSession.commit()
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        add_edit_decorative_type_menu = AddEditDecorativeTypeMenu(self)
        return {self.States.ACTION: [add_edit_decorative_type_menu.handler,
                                     CallbackQueryHandler(self.edit_visible, pattern='^edit_visible$')]}

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END


class AddEditDecorativeTypeMenu(ArrowAddEditMenu):
    menu_name = "add_edit_paint_menu"
    model = ProductDecorativeCoatings
    field_buttons_group_size = 2
    show_reset_field = True
    use_markdown_text = False
    use_html_text = False

    def entry(self, update, context):
        self._entry(update, context)
        data = update.callback_query.data
        if 'edit_decorative_type_' in data:
            context.user_data[self.menu_name]['decorative_type_id'] = int(data.replace("edit_decorative_type_", ""))
        else:
            context.user_data[self.menu_name]['decorative_type_id'] = None

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        if context.user_data[self.menu_name]['decorative_type_id'] is not None:
            return DBSession.query(ProductDecorativeCoatings).get(context.user_data[self.menu_name]['decorative_type_id'])
        else:
            return None

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_decorative_type$"),
                CallbackQueryHandler(self.entry, pattern=r"^edit_decorative_type_\d+$")]

    def fields(self, context):
        _ = context.user_data['_']
        return [self.Field('name*', _("Name"), validators.String()),
                self.Field('description*', _("Description"), validators.String()),
                self.Field('vlastivosti*', _("Features"), validators.String()),
                self.Field('density', _("Density"), validators.String()),
                self.Field('consumption', _("Consumption"), validators.String()),
                self.Field('color_shade', _("Color"), validators.String()),
                self.Field('gloss', _("Gloss"), validators.String()),
                self.Field('packing', _("Packing"), validators.String()),
                self.Field('product_photo', _("Photo"), validators.String(), message_type=MessageType.photo),
                self.Field('ti', _("PDF"), validators.String(), message_type=MessageType.document),
                self.Field('language_code*', _("Language"), validators.String(), variants=['ru', 'uk'])]

    def fill_field(self, update, context):
        delete_interface(context)
        return super(AddEditDecorativeTypeMenu, self).fill_field(update, context)

    def save_object(self, obj, context, session=None):
        obj.category = "Декоративні покриття"
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def back(self, update, context):
        self.parent.update_objects(context)
        delete_interface(context)
        self.parent.send_message(context)
        return ConversationHandler.END
