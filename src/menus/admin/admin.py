import datetime
import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import translator, unknown_command, to_state, get_settings, prepare_user
from botmanlib.messages import delete_interface, send_or_edit
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import ConversationHandler, CommandHandler, Filters, MessageHandler, CallbackQueryHandler

from src.menus.admin.contacts import AdminContactsMenu
from src.menus.admin.distributions import DistributionsMenu
from src.menus.admin.products_decorative_types import DecorativeTypesAdminMenu
from src.menus.admin.products_first_coats import FirstCoatsAdminMenu
from src.menus.admin.products_heat_systems import HeatSystemsAdminMenu
from src.menus.admin.products_paints import PaintsAdminMenu
from src.menus.admin.products_putty_and_pluster import PuttyAndPlusterAdminMenu
from src.models import DBSession, User
from src.settings import SETTINGS_FILE, ADMIN_PASSWORD


class AdminMenu(BaseMenu):
    menu_name = 'admin_menu'

    class States(enum.Enum):
        ACTION = 1
        PASSWORD = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context, lang='uk')
        context.user_data['user'] = user

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)

        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)

        return AdminMenu.States.ACTION

    def ask_password(self, update, context):
        user = prepare_user(User, update, context, lang='uk')
        context.user_data['user'] = user
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        buttons = [[InlineKeyboardButton(_("Return to main menu"), callback_data='start')]]

        delete_interface(context)
        mes = context.bot.send_message(chat_id=user.chat_id, text=_("Enter admin password:"), reply_markup=ReplyKeyboardRemove())
        mes.done.wait()
        context.bot.delete_message(chat_id=user.chat_id, message_id=mes.result().message_id)
        send_or_edit(context, chat_id=user.chat_id, text=_("Enter admin password:"), reply_markup=InlineKeyboardMarkup(buttons))

        return AdminMenu.States.PASSWORD

    def check_pass(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        text = update.message.text

        data = get_settings(SETTINGS_FILE)
        if 'password' in data:
            password = data['password']
        else:
            password = ADMIN_PASSWORD

        if text != password:
            return self.ask_password(update, context)

        delete_interface(context)
        self.send_message(context)

        return AdminMenu.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Distribution"), callback_data='distribution')],
                   [InlineKeyboardButton(_("Paints"), callback_data='paints')],
                   [InlineKeyboardButton(_("First coats"), callback_data='first_coats')],
                   [InlineKeyboardButton(_("Putty and pluster"), callback_data='putty_and_pluster')],
                   [InlineKeyboardButton(_("Decorative types"), callback_data='decorative_types')],
                   [InlineKeyboardButton(_("Heat systems"), callback_data='heat_systems')],
                   [InlineKeyboardButton(_("Statistics"), callback_data='statistics')],
                   [InlineKeyboardButton(_("Contacts"), callback_data='edit_contacts')],
                   [InlineKeyboardButton(_("Goto main menu"), callback_data='goto_start')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(buttons))

    def goto_start(self, update, context):
        update.callback_query.data = 'start'
        context.update_queue.put(update)
        return ConversationHandler.END

    def statistics(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        all_users = DBSession.query(func.count(User.id)).first()[0]
        day_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=1)).first()[0]
        week_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=7)).first()[0]
        month_joined = DBSession.query(func.count(User.id)).filter(User.join_date > datetime.datetime.utcnow() - datetime.timedelta(days=30)).first()[0]
        block_users = DBSession.query(func.count(User.id)).filter(User.is_active == False).first()[0]

        message_text = _("Users in bot:") + f" {all_users}\n"
        message_text += _("Joined in past 1 day:") + f" {day_joined}\n"
        message_text += _("Joined in past 7 days:") + f" {week_joined}\n"
        message_text += _("Joined in past 30 days:") + f" {month_joined}\n"
        message_text += _("Users that stop or block bot:") + f" {block_users}\n"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

        return AdminMenu.States.ACTION

    def get_handler(self):
        paints_menu = PaintsAdminMenu(self)
        first_coats_menu = FirstCoatsAdminMenu(self)
        pnp_menu = PuttyAndPlusterAdminMenu(self)
        decorative_types_menu = DecorativeTypesAdminMenu(self)
        heat_systems_menu = HeatSystemsAdminMenu(self)
        contacts_menu = AdminContactsMenu(self)
        distribution_menu = DistributionsMenu(self)


        handler = ConversationHandler(entry_points=[CommandHandler('admin', self.ask_password)],
                                      states={
                                          AdminMenu.States.ACTION: [distribution_menu.handler,
                                                                    paints_menu.handler,
                                                                    first_coats_menu.handler,
                                                                    pnp_menu.handler,
                                                                    decorative_types_menu.handler,
                                                                    heat_systems_menu.handler,
                                                                    contacts_menu.handler,
                                                                    CallbackQueryHandler(self.statistics, pattern='^statistics$'),
                                                                    CallbackQueryHandler(self.entry, pattern='^back$'),
                                                                    CallbackQueryHandler(self.goto_start, pattern='^goto_start$'),
                                                                    CommandHandler('start', self.goto_start),
                                                                    MessageHandler(Filters.all, to_state(AdminMenu.States.ACTION))],
                                          AdminMenu.States.PASSWORD: [MessageHandler(Filters.text, self.check_pass),
                                                                      MessageHandler(Filters.all, to_state(AdminMenu.States.PASSWORD))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
