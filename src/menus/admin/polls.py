import enum

import formencode
from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import group_buttons, add_to_db
from botmanlib.messages import delete_interface, delete_user_message
from botmanlib.models import Database, MessageType
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.error import Unauthorized, BadRequest
from telegram.ext import CallbackQueryHandler
from telegram.utils.promise import Promise

from src.models import DBSession, Poll, PollAnswer, User, JobSession, PollMessage


class PollDistributionMenu(OneListMenu):
    menu_name = "poll_distribution_menu"
    parse_mode = ParseMode.MARKDOWN_V2
    add_delete_button = True

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^poll_distribution$")]

    def query_objects(self, context):
        polls = DBSession.query(Poll).order_by(Poll.create_date.desc()).all()
        return polls

    def message_text(self, context, poll):
        user = context.user_data['user']
        _ = user.translator
        message_text = ""

        if poll:
            message_text += "**" + _("Poll #{poll_id}").format(poll_id=poll.id) + "**\n\n"
            message_text += _("Question:") + f" {poll.question}\n"
            message_text += _("Answers:") + "\n"
            for idx, answer in enumerate(poll.answers):
                message_text += f"{idx + 1}. {answer.text} - {len(answer.users)}\n"

            message_text += _("Sent:") + " " + (_("Yes") if poll.sent else _("No")) + "\n"
        else:
            message_text += _("There is no polls yet")

        message_text = message_text.replace(".", "\.")
        message_text = message_text.replace("-", "\-")
        return message_text

    def object_buttons(self, context, poll):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        buttons.append([InlineKeyboardButton(_("Create poll"), callback_data="add_new_poll")])
        if poll and poll.sent is False:
            buttons.append([InlineKeyboardButton(_("Send poll"), callback_data="send_poll")])

        return buttons

    def start_distribution(self, update, context):
        _ = context.user_data['user'].translator

        poll = self.selected_object(context)
        job_context = {'user_data': context.user_data,
                       'update_queue': context.update_queue,
                       'poll_id': poll.id}

        context.job_queue.run_once(self.distribution_job, 0, context=job_context)

        context.bot.answer_callback_query(update.callback_query.id, text=_("Poll was sent to all bot users."), show_alert=True)
        poll = self.selected_object(context)
        poll.sent = True
        add_to_db(poll, DBSession)

        self.send_message(context)
        return self.States.ACTION

    def users_to_send(self, context):
        session = Database().sessionmaker()
        return session.query(User).filter(User.is_active == True).filter(User.chat_id != context.user_data["user"].chat_id).all()

    def distribution_job(self, context):
        context._user_data = context.job.context['user_data']
        users = self.users_to_send(context)
        poll_id = context.job.context['poll_id']
        poll = JobSession.query(Poll).get(poll_id)

        for user in users:
            flat_buttons = []
            for answer in poll.answers:
                flat_buttons.append(InlineKeyboardButton(f"{answer.text} - 0", callback_data=f"answer_poll_{answer.id}"))
            buttons = group_buttons(flat_buttons, 2)

            message_text = poll.question
            message_text = message_text.replace(".", "\.")
            message_text = message_text.replace("-", "\-")

            try:
                mes = context.bot.send_message(chat_id=user.chat_id, text=message_text, disable_web_page_preview=True, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=ParseMode.MARKDOWN_V2)
                if isinstance(mes, Promise):
                    mes.done.wait()
                    mes = mes.result()
                JobSession.add(PollMessage(chat_id=user.chat_id, message_id=mes.message_id, poll_id=poll_id))
                self.logger.info(f"Distribution message was sent to user {user.chat_id}")
            except (Unauthorized, BadRequest):
                self.logger.info(f"Distribution message was NOT sent to user {user.chat_id}")
                continue

        job_context = {"poll_id": poll_id}
        context.job_queue.run_once(self.update_poll_job, 60, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 2, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 5, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 10, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 30, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 2, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 3, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 4, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 5, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 6, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 7, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 8, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 9, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 10, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 11, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 12, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 15, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 17, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 20, context=job_context)
        context.job_queue.run_once(self.update_poll_job, 60 * 60 * 24, context=job_context)

    def update_poll_job(self, context):
        poll_id = context.job.context['poll_id']
        poll = JobSession.query(Poll).get(poll_id)

        for message in poll.messages:
            flat_buttons = []
            for answer in poll.answers:
                flat_buttons.append(InlineKeyboardButton(f"{answer.text} - {len(answer.users)}", callback_data=f"answer_poll_{answer.id}"))
            buttons = group_buttons(flat_buttons, 2)

            markup = InlineKeyboardMarkup(buttons)
            try:
                context.bot.edit_message_reply_markup(chat_id=message.chat_id, message_id=message.message_id, reply_markup=markup)
            except BadRequest:
                pass
        self.logger.info(f"Updated poll messages from {poll.create_date.strftime('%d.%m.%Y %H:%M')}")

    def additional_states(self):
        add_poll_menu = AddPollMenu(self)
        return {self.States.ACTION: [add_poll_menu.handler,
                                     CallbackQueryHandler(self.start_distribution, pattern="^send_poll$")]}


class AddPollMenu(ArrowAddEditMenu):
    menu_name = "add_poll_menu"
    force_action = ArrowAddEditMenu.Action.ADD
    model = dict
    parse_mode = ParseMode.MARKDOWN
    message_parse_mode = ParseMode.MARKDOWN

    def query_object(self, context):
        return None

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('question', "\*" + _("Question"), validators.String(), required=True),
                  self.Field('answer1', "\*" + _("Answer 1"), validators.String(), required=True),
                  self.Field('answer2', "\*" + _("Answer 2"), validators.String(), required=True),
                  self.Field('answer3', _("Answer 3"), validators.String(), required=False, depend_field="answer2", depend_inverse=True, depend_value=None),
                  self.Field('answer4', _("Answer 4"), validators.String(), required=False, depend_field="answer3", depend_inverse=True, depend_value=None),
                  self.Field('answer5', _("Answer 5"), validators.String(), required=False, depend_field="answer4", depend_inverse=True, depend_value=None),
                  self.Field('answer6', _("Answer 6"), validators.String(), required=False, depend_field="answer5", depend_inverse=True, depend_value=None),
                  self.Field('answer7', _("Answer 7"), validators.String(), required=False, depend_field="answer6", depend_inverse=True, depend_value=None),
                  self.Field('answer8', _("Answer 8"), validators.String(), required=False, depend_field="answer7", depend_inverse=True, depend_value=None),
                  self.Field('answer9', _("Answer 9"), validators.String(), required=False, depend_field="answer8", depend_inverse=True, depend_value=None),
                  ]
        return fields

    def save_object(self, poll_data, context, session=None):

        poll = Poll(question=poll_data['question'])
        for i in range(1, 10):
            if context.user_data[self.menu_name][f'answer{i}']:
                poll_answer = PollAnswer()
                poll_answer.text = context.user_data[self.menu_name][f'answer{i}']
                poll_answer.poll = poll
                DBSession.add(poll_answer)

        DBSession.commit()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_new_poll$")]

    def fill_field(self, update, context):
        user_data = context.user_data
        user = user_data['user']
        _ = user.translator
        # check is there is any active field to fill
        fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']
        if self.remove_user_messages:
            delete_user_message(update)
        if active_field is None:
            self.send_message(context)
            return self.States.ACTION
        if fields[active_field].variants and update.callback_query is None:
            self.send_message(context)
            return self.States.ACTION

        if fields[active_field].message_type is MessageType.document:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me document"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.photo:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me photo"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.video:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me video"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.animation:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me animation"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.sticker:
            self.bot.send_message(chat_id=user.chat_id, text=_("Please send me sticker"))
            self.send_message(context)
            return self.States.ACTION
        elif fields[active_field].message_type is MessageType.location:
            # do nothing
            return self.States.ACTION

        # obtain text
        if update.callback_query:
            data = int(update.callback_query.data.replace("variant_", ""))
            text = self.active_field(context).variants[data]
            delete_interface(context, 'variations_interface')
            self.bot.answer_callback_query(update.callback_query.id)
        else:
            if self.message_parse_mode == ParseMode.MARKDOWN:
                if fields[active_field].param == 'question':
                    text = update.message.text_markdown
                else:
                    text = update.message.text

            elif self.message_parse_mode == ParseMode.HTML:
                text = update.message.text_html
            else:
                text = update.message.text

        # write text to user_data
        available_fields = self._available_fields(context)
        field = available_fields[active_field]
        try:
            if field.multiple and field.variants:
                if user_data[self.menu_name][field.param] is None or user_data[self.menu_name][field.param] is []:
                    user_data[self.menu_name][field.param] = []
                value = field.validate(text)
                if value in user_data[self.menu_name][field.param]:
                    user_data[self.menu_name][field.param].remove(value)
                else:
                    user_data[self.menu_name][field.param].append(value)

                self.send_message(context)
                return self.States.ACTION
            else:
                user_data[self.menu_name][field.param] = field.validate(text)

            user_data[self.menu_name]['variant_page'] = 0

        except formencode.Invalid:
            delete_interface(context)

            self.bot.send_message(chat_id=user.chat_id, text=field.invalid_message if field.invalid_message else _("Wrong input, try again"), parse_mode=self.parse_mode)
            self.send_message(context)
            return self.States.ACTION

        # change selected field
        return self.next_field(update, context)
