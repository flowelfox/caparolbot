import enum

from botmanlib.menus.helpers import group_buttons, get_settings, write_settings
from botmanlib.menus.list_menus.bunch_list_menu import BunchListMenu
from botmanlib.messages import delete_interface
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.settings import SETTINGS_FILE


class CertificatesMenu(BunchListMenu):
    menu_name = 'certificates_menu'
    objects_per_page = 10
    auto_hide_arrows = True
    send_as_markdown = True

    class States(enum.Enum):
        ACTION = 1

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='certificates')]

    def back(self, update, context):
        context.bot.answer_callback_query(update.callback_query.id)

        self.parent.send_message(context)
        return ConversationHandler.END

    def query_objects(self, context):
        user = context.user_data['user']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        return list(settings["health_and_safety"][language_code]["certificates"].keys())

    def page_text(self, current_page, max_page, user_data):
        return ""

    def message_text(self, context, page_objects):
        _ = context.user_data['_']

        if page_objects:
            message_text = ""
            for obj in page_objects:
                message_text += obj + '\n'
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def object_buttons(self, context, page_objects):
        _ = context.user_data['_']

        if page_objects:
            buttons_flat = []
            for obj in page_objects:
                index = obj.rindex("<")
                buttons_flat.append(InlineKeyboardButton(obj[3:index], callback_data=f'{obj[:index]}'))
            buttons = group_buttons(buttons_flat, 2)
        else:
            buttons = []

        return buttons

    def download_file(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_list")]]

        settings = get_settings(SETTINGS_FILE)
        keys = settings["health_and_safety"][language_code]["certificates"].keys()
        for key in keys:
            if update.callback_query.data in key:
                delete_interface(context)
                mes = context.bot.send_document(chat_id=user.chat_id, document=settings["health_and_safety"][language_code]["certificates"][key], reply_markup=InlineKeyboardMarkup(buttons))
                context.user_data["interface"] = mes
                settings["health_and_safety"][language_code]["certificates"][key] = mes.document.file_id
        write_settings({"health_and_safety": settings["health_and_safety"]}, SETTINGS_FILE)
        return self.States.ACTION

    def back_to_list(self, update, context):
        self.send_message(context)
        return CertificatesMenu.States.ACTION

    def additional_states(self):

        return {CertificatesMenu.States.ACTION: [CallbackQueryHandler(self.download_file, pattern='^<b>'),
                                                 CallbackQueryHandler(self.back_to_list, pattern='^back_to_list$')]}
