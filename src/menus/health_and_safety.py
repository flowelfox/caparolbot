import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import unknown_command, to_state, get_settings
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.certificates import CertificatesMenu
from src.settings import SETTINGS_FILE


class HealthAndSafetyMenu(BaseMenu):
    menu_name = 'health_and_safety_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        message_text = _("Select an action, {name}.").format(name=user.desired_name)

        buttons = [[InlineKeyboardButton(_("Eco standard ELF"), callback_data='get_elf')],
                   [InlineKeyboardButton(_("Impact VOC"), callback_data='get_impact')],
                   [InlineKeyboardButton(_("Doctor's advice"), callback_data='get_advices')],
                   [InlineKeyboardButton(_("Safe Caparol colors"), callback_data='get_save_colors')],
                   [InlineKeyboardButton(_("Certificates"), callback_data='certificates')],
                   [InlineKeyboardButton(_("Back"), callback_data='back')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def show_info(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if user.language_code == "ru":
            language_code = "ru"
        else:
            language_code = "ua"
        settings = get_settings(SETTINGS_FILE)

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=settings["health_and_safety"][language_code][update.callback_query.data.replace("get_", "")], reply_markup=InlineKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")

        context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        context.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        certificates_menu = CertificatesMenu(self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^safety_and_health$")],
                                      states={self.States.ACTION: [CallbackQueryHandler(self.entry, pattern="^back_to_menu$"),
                                                                   CallbackQueryHandler(self.show_info, pattern="^get_\w+"),
                                                                   certificates_menu.handler,
                                                                   CallbackQueryHandler(self.back, pattern="^back$"),
                                                                   MessageHandler(Filters.all, to_state(self.States.ACTION))]
                                              },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)
        return handler
