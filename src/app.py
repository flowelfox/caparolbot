import datetime
import logging
import os
import pprint
import re
import traceback
from gettext import gettext

import coloredlogs
from botmanlib.bot import BotmanBot
from botmanlib.menus.helpers import prepare_user, group_buttons
from botmanlib.updater import BotmanUpdater
from telegram import Message, Chat, TelegramError, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import BadRequest
from telegram.ext import MessageHandler, Filters, CommandHandler, messagequeue, CallbackQueryHandler
from telegram.utils.request import Request

from src.jobs import start_check_blocked_job, stop_check_blocked_job
from src.menus.admin.admin import AdminMenu
from src.menus.start import StartMenu
from src.models import User, DBSession, PollAnswer, Distribution, Reaction
from src.settings import MEDIA_FOLDER, RESOURCES_FOLDER, SETTINGS_FILE, WEBHOOK_PORT, WEBHOOK_IP, WEBHOOK_URL, WEBHOOK_ENABLE, KEY, CERT

logger = logging.getLogger(__name__)


def stop(update, context):
    # if user never have a conversation with bot and sending /stop he has empty user_data
    if context.user_data:
        _ = context.context.user_data['_'] if '_' in context.user_data else gettext
        user = DBSession.query(User).filter(User.chat_id == context.context.user_data['user'].chat_id).first()
        user.block_date = datetime.datetime.utcnow()
        user.chat_room_id = None
        DBSession.add(user)
        DBSession.commit()
        context.bot.send_message(chat_id=user.chat_id, text=_("You are deleted from this bot."))


def error(update, context):
    """Log Errors caused by Updates."""
    pp = pprint.PrettyPrinter(indent=4)
    logger.error(f'Update "{pp.pformat(str(update))}" caused error "{context.error}"')
    traceback.print_exc()


def goto_start(update, context):
    if update.effective_message and update.effective_message.message_id != 0:
        try:
            context.bot.delete_message(chat_id=update.effective_chat.id, message_id=update.effective_message.message_id)
        except TelegramError as e:
            logger.error(str(e))

    update.message = Message(0, update.effective_user, datetime.datetime.utcnow(), Chat(0, Chat.PRIVATE), text='/start', bot=context.bot)
    update._effective_message = None
    update.callback_query = None
    context.update_queue.put(update)


def process_ok(update, context):
    try:
        context.bot.delete_message(chat_id=update.effective_chat.id, message_id=update.effective_message.message_id)
    except TelegramError as e:
        logger.error(str(e))


#
#
# def update_reaction(update, context):
#     user = prepare_user(User, update, context, lang='uk')
#     _ = user.translator
#     data = update.callback_query.data
#     reply_markup = update.effective_message.reply_markup
#     message_id = update.effective_message.message_id
#
#     if "reacted_to" not in context.user_data:
#         context.user_data['reacted_to'] = {}
#
#     if context.user_data['reacted_to'].get(message_id, None) == data:
#         update.callback_query.answer(_("You already added your reaction"))
#         return
#     elif context.user_data['reacted_to'].get(message_id, None) != data:
#         old_data = context.user_data['reacted_to'].get(message_id)
#         # remove old reaction
#         for buttons_group in reply_markup.inline_keyboard:
#             for button in buttons_group:
#                 if button.callback_data == old_data:
#                     reaction, number = re.findall(r"(.+) - (\d+)", button.text)[0]
#                     new_text = f"{reaction} - {int(number) - 1}"
#                     button.text = new_text
#
#     # add new reaction
#     context.user_data['reacted_to'].update({message_id: data})
#     for buttons_group in reply_markup.inline_keyboard:
#         for button in buttons_group:
#             if button.callback_data == data:
#                 reaction, number = re.findall(r"(.+) - (\d+)", button.text)[0]
#                 new_text = f"{reaction} - {int(number) + 1}"
#                 button.text = new_text
#
#     update.callback_query.answer()
#     update.effective_message.edit_reply_markup(reply_markup=reply_markup)

def update_reaction(update, context):
    user = prepare_user(User, update, context, lang='uk')
    _ = user.translator
    data = update.callback_query.data

    reaction_id = int(data.replace("update_reaction_", ""))
    reaction = DBSession.query(Reaction).get(reaction_id)

    if user in reaction.users:
        reaction.users.remove(user)
        update.callback_query.answer()
        return
    else:
        for r in reaction.distribution.reactions:
            try:
                r.users.remove(user)
            except ValueError:
                pass
            DBSession.add(r)

        reaction.users.append(user)
        DBSession.add(reaction)
    DBSession.commit()

    flat_buttons = []
    for idx, reaction in enumerate(reaction.distribution.reactions):
        flat_buttons.append(InlineKeyboardButton(f"{reaction.text} - {len(reaction.users)}", callback_data=f"update_reaction_{reaction.id}"))

    buttons = group_buttons(flat_buttons, 4)
    markup = InlineKeyboardMarkup(buttons)

    try:
        update.effective_message.edit_reply_markup(reply_markup=markup)
    except BadRequest:
        pass

    update.callback_query.answer()


def poll_answer(update, context):
    user = context.user_data.get("user", None)
    if user is None:
        user = prepare_user(User, update, context, lang='uk')

    _ = user.translator

    answer_id = int(update.callback_query.data.replace("answer_poll_", ""))
    answer = DBSession.query(PollAnswer).get(answer_id)
    already_answered = False
    for a in answer.poll.answers:
        if user in a.users:
            already_answered = True

    if already_answered:
        update.callback_query.answer(text=_("You already answered this on this poll"), show_alert=True)
    else:
        answer.users.append(user)
        DBSession.add(answer)
        DBSession.commit()

        flat_buttons = []

        for a in answer.poll.answers:
            flat_buttons.append(InlineKeyboardButton(f"{a.text} - {len(a.users)}", callback_data=f"answer_poll_{a.id}"))
        buttons = group_buttons(flat_buttons, 2)

        markup = InlineKeyboardMarkup(buttons)

        try:
            update.effective_message.edit_reply_markup(reply_markup=markup)
        except BadRequest:
            pass

        update.callback_query.answer(text=_("Your answer registered"), show_alert=True)



def main():
    bot_token = os.environ['bot.token']
    mqueue = messagequeue.MessageQueue(all_burst_limit=30, all_time_limit_ms=1000)
    request = Request(con_pool_size=8)

    bot = BotmanBot(token=bot_token, request=request, mqueue=mqueue)
    updater = BotmanUpdater(bot=bot, use_context=True, use_sessions=True)

    job_queue = updater.job_queue
    dispatcher = updater.dispatcher
    coloredlogs.install()

    # Handlers
    start_menu = StartMenu(bot=bot, dispatcher=dispatcher)
    admin_menu = AdminMenu(bot=bot, dispatcher=dispatcher)
    stop_handler = CommandHandler('stop', stop)

    # adding menus
    dispatcher.add_handler(CallbackQueryHandler(process_ok, pattern='^ok$'))
    dispatcher.add_handler(CallbackQueryHandler(update_reaction, pattern='^update_reaction_\d+$'))
    dispatcher.add_handler(CallbackQueryHandler(poll_answer, pattern='^answer_poll_\d+$'))
    dispatcher.add_handler(stop_handler)
    dispatcher.add_handler(start_menu.handler)
    dispatcher.add_handler(admin_menu.handler)
    dispatcher.add_handler(MessageHandler(Filters.all, goto_start))
    dispatcher.add_handler(CallbackQueryHandler(goto_start))
    dispatcher.add_error_handler(error)

    # Start bot
    if not os.path.exists(MEDIA_FOLDER):
        os.mkdir(MEDIA_FOLDER)
        logger.info("Media folder created")
    if not os.path.exists(RESOURCES_FOLDER):
        os.mkdir(RESOURCES_FOLDER)
        logger.info("Resources folder created")
    if not os.path.exists(SETTINGS_FILE):
        with open(SETTINGS_FILE + ".default", 'r') as settings:
            out = open(SETTINGS_FILE, 'w')
            out.write(settings.read())
            out.close()
        logger.info("Settings file created")

    if WEBHOOK_ENABLE:
        update_queue = updater.start_webhook(cert=CERT, key=KEY, listen=WEBHOOK_IP, port=WEBHOOK_PORT, webhook_url=WEBHOOK_URL)
    else:
        update_queue = updater.start_polling(allowed_updates=['message', 'edited_message', 'callback_query'])

    start_check_blocked_job(job_queue)

    logger.info("Bot started")
    updater.idle()

    stop_check_blocked_job(job_queue)
    bot.stop()


if __name__ == "__main__":
    main()
