import json
import logging
import os
from time import sleep

import requests
from bs4 import BeautifulSoup, Tag, NavigableString
from selenium import webdriver
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from src.models import DBSession, ProductCategory, ProductFirstCoats, ProductPaints, ProductPuttyAndPlaster, ProductDecorativeCoatings, ProductHeatSystems
from src.settings import RESOURCES_FOLDER

COLLECTED_DATA_FILE = 'collected_data.json'
UPDATE_LINKS = False

UPDATE_FIRST_COATS = False
UPDATE_PAINTS = False
UPDATE_P_AND_P = False
UPDATE_DECORATIVE = False
UPDATE_HEAT_SYSTEMS = True

HEADLESS = True

base_url = 'http://www.caparol.ua'

logger = logging.getLogger(__name__)


def get_links(browser, file_path):
    """Parse all product links"""
    browser.get(base_url + "/produkti/alfavitnii-vkazivnik.html")

    browser.execute_script('window.results = [];')
    script = """
    var pimIDS = %product_id%;
    $.ajax({
                type: "POST",
                url: window.location.pathname,
                data: {
                    eID: "getproductdata", request : {
                        action: "getCategory", arguments: {
                            getproduct: pimIDS
                        }
                    }
                },
                dataType: "html",
                success: function(result) {
                    window.results.push(result);
                },
                error: function(error) {
                    window.results.push(error);
                }
            });
    """

    ids = [title.get_attribute("id") for title in browser.find_elements_by_xpath("//h4[contains(@class, 'panel-title')]")]

    for id in ids:
        browser.execute_script(script.replace('%product_id%', id))

    data = []
    while len(data) < len(ids):
        data = browser.execute_script('return window.results')

        sleep(1)
        print(f"Collected {len(data)} results")

    links = []
    for html in data:
        soup = BeautifulSoup(html, 'html.parser')
        links.append(base_url + '/' + soup.find('a', string='Дізнитися більше').get('href'))

    write_collected_data({'links': links}, file_path)
    return links


def write_collected_data(new_data: dict, file_path=COLLECTED_DATA_FILE):
    """Write dict to file"""
    data = get_collected_data(file_path)
    data.update(new_data)

    with open(file_path, 'w') as f:
        json.dump(data, f)


def get_collected_data(file_path=COLLECTED_DATA_FILE):
    """Get all data which was saved to file"""
    if not os.path.exists(file_path):
        return {}
    with open(file_path, 'r') as f:
        data = json.load(f)
    return data


def parse_putty_and_plaster(prod, browser):
    setattr(prod, "name", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').text)
    logger.info(f"Parsing {prod.name}")
    temp = DBSession.query(ProductPuttyAndPlaster).filter(ProductPuttyAndPlaster.name == prod.name).first()
    if temp:
        logger.info("Object already exist")
        return None
    else:
        description = soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > p')
        if description:
            setattr(prod, "description", description.text)

        else:
            setattr(prod, "description", soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').next_sibling)
        setattr(prod, "product_photo", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-4.product-image > img').get_attribute("src"))
        setattr(prod, "category", "Шпаклівки та штукатурки")
        if soup.find("a", string="Опис продукту"):
            div = soup.select_one('#tab1 > div > div.row > div')

            active_field = None

            for e in div.contents:
                if isinstance(e, Tag):
                    if e.name == 'h3' and e.text == 'Властивості':
                        active_field = 'vlastivosti'
                        continue
                    elif e.name == 'h3' and (e.text == 'Колірні відтінки' or e.text == 'Віддітки фарб'):
                        active_field = 'color_shade'
                        continue
                    elif e.name == 'h3' and e.text == 'Упаковка/Розмір тари':
                        active_field = 'packing'
                        continue
                    elif e.name == 'h3' and e.text == 'Ступінь глянцю':
                        active_field = 'gloss'
                        continue
                    elif e.name == 'h3' and e.text == 'Максимальна величина зерна':
                        active_field = 'grain'
                        continue
                    elif e.name == 'h3':
                        active_field = None

                    if active_field is not None:
                        if e.name == 'ul' or e.name == 'ol':
                            text = "\n".join([str(c).replace('<li>', '- ').replace("</li>", "") for c in e.contents])
                        else:
                            text = str(e)
                        text = text.replace('<p>', '').replace('</p>', '')
                        text = text.replace('<strong>', '').replace('</strong>', '')
                        text = text.replace('<sub>', '').replace('</sub>', '')
                        text = text.replace('<sup>', '').replace('</sup>', '')
                        text = text.replace('<br/>', '\n')
                        text = text.replace('\n\n\n', '\n\n')
                        try:
                            text = text[:text.index('<table')] + text[text.index('</table>') + 8:]
                        except ValueError as ex:
                            pass
                        if getattr(prod, active_field):
                            text = '\n' + text
                        value = getattr(prod, active_field)
                        value = value if value is not None else ""
                        setattr(prod, active_field, value + text)

                if isinstance(e, NavigableString) and e is not None and active_field is not None:
                    value = getattr(prod, active_field)
                    value = value if value is not None else ""
                    setattr(prod, active_field, value + '\n' + str(e))
            ti = soup.select_one('#tab4 > div > div > div > ul > li > a')
            if ti is not None:
                setattr(prod, "ti", base_url + "/" + ti.get("href"))
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

        return prod


def parse_paints(prod, browser):
    setattr(prod, "name", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').text)
    logger.info(f"Parsing {prod.name}")
    temp = DBSession.query(ProductPaints).filter(ProductPaints.name == prod.name).first()
    if temp:
        logger.info("Object already exist")
        return None
    else:
        description = soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > p')
        if description:
            setattr(prod, "description", description.text)

        else:
            setattr(prod, "description", soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').next_sibling)
        setattr(prod, "product_photo", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-4.product-image > img').get_attribute("src"))
        if soup.find("a", string="Опис продукту"):
            div = soup.select_one('#tab1 > div > div.row > div')

            active_field = None

            for e in div.contents:
                if isinstance(e, Tag):
                    if e.name == 'h3' and e.text == 'Властивості':
                        active_field = 'vlastivosti'
                        continue
                    elif e.name == 'h3' and (e.text == 'Колірні відтінки' or e.text == 'Віддітки фарб'):
                        active_field = 'color_shade'
                        continue
                    elif e.name == 'h3' and e.text == 'Упаковка/Розмір тари':
                        active_field = 'packing'
                        continue
                    elif e.name == 'h3' and e.text == 'Ступінь глянцю':
                        active_field = 'gloss'
                        continue
                    elif e.name == 'h3' and e.text == 'Щільність':
                        active_field = 'density'
                        continue
                    elif e.name == 'h3':
                        active_field = None

                    if active_field is not None:
                        if e.name == 'ul' or e.name == 'ol':
                            text = "\n".join([str(c).replace('<li>', '- ').replace("</li>", "") for c in e.contents])
                        else:
                            text = str(e)
                        text = text.replace('<p>', '').replace('</p>', '')
                        text = text.replace('<strong>', '').replace('</strong>', '')
                        text = text.replace('<sub>', '').replace('</sub>', '')
                        text = text.replace('<sup>', '').replace('</sup>', '')
                        text = text.replace('<br/>', '\n')
                        text = text.replace('\n\n\n', '\n\n')
                        try:
                            text = text[:text.index('<table')] + text[text.index('</table>') + 8:]
                        except ValueError as ex:
                            pass
                        if getattr(prod, active_field):
                            text = '\n' + text
                        value = getattr(prod, active_field)
                        value = value if value is not None else ""
                        setattr(prod, active_field, value + text)

                if isinstance(e, NavigableString) and e is not None and active_field is not None:
                    value = getattr(prod, active_field)
                    value = value if value is not None else ""
                    setattr(prod, active_field, value + '\n' + str(e))
            ti = soup.select_one('#tab4 > div > div > div > ul > li > a')
            if ti is not None:
                setattr(prod, "ti", base_url + "/" + ti.get("href"))
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

        return prod


def parse_first_coats(prod, browser):
    setattr(prod, "name", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').text)
    logger.info(f"Parsing {prod.name}")
    temp = DBSession.query(ProductFirstCoats).filter(ProductFirstCoats.name == prod.name).first()
    if temp:
        logger.info("Object already exist")
        return None
    else:
        description = soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > p')
        if description:
            setattr(prod, "description", description.text)

        else:
            setattr(prod, "description", soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').next_sibling)
        setattr(prod, "product_photo", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-4.product-image > img').get_attribute("src"))
        setattr(prod, "category", "Ґрунтовки")
        if soup.find("a", string="Опис продукту"):
            div = soup.select_one('#tab1 > div > div.row > div')

            active_field = None

            for e in div.contents:
                if isinstance(e, Tag):
                    if e.name == 'h3' and e.text == 'Властивості':
                        active_field = 'vlastivosti'
                        continue
                    elif e.name == 'h3' and (e.text == 'Колірні відтінки' or e.text == 'Віддітки фарб'):
                        active_field = 'color_shade'
                        continue
                    elif e.name == 'h3' and e.text == 'Упаковка/Розмір тари':
                        active_field = 'packing'
                        continue
                    elif e.name == 'h3' and e.text == 'Ступінь глянцю':
                        active_field = 'gloss'
                        continue
                    elif e.name == 'h3' and e.text == 'Щільність':
                        active_field = 'density'
                        continue
                    elif e.name == 'h3':
                        active_field = None

                    if active_field is not None:
                        if e.name == 'ul' or e.name == 'ol':
                            text = "\n".join([str(c).replace('<li>', '- ').replace("</li>", "") for c in e.contents])
                        else:
                            text = str(e)
                        text = text.replace('<p>', '').replace('</p>', '')
                        text = text.replace('<strong>', '').replace('</strong>', '')
                        text = text.replace('<sub>', '').replace('</sub>', '')
                        text = text.replace('<sup>', '').replace('</sup>', '')
                        text = text.replace('<br/>', '\n')
                        text = text.replace('\n\n\n', '\n\n')
                        try:
                            text = text[:text.index('<table')] + text[text.index('</table>') + 8:]
                        except ValueError as ex:
                            pass
                        if getattr(prod, active_field):
                            text = '\n' + text
                        value = getattr(prod, active_field)
                        value = value if value is not None else ""
                        setattr(prod, active_field, value + text)

                if isinstance(e, NavigableString) and e is not None and active_field is not None:
                    value = getattr(prod, active_field)
                    value = value if value is not None else ""
                    setattr(prod, active_field, value + '\n' + str(e))
            ti = soup.select_one('#tab4 > div > div > div > ul > li > a')
            if ti is not None:
                setattr(prod, "ti", base_url + "/" + ti.get("href"))
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

        return prod


def parse_decorative_coatings(prod, browser):
    setattr(prod, "name", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').text)
    logger.info(f"Parsing {prod.name}")
    temp = DBSession.query(ProductDecorativeCoatings).filter(ProductDecorativeCoatings.name == prod.name).first()
    if temp:
        logger.info("Object already exist")
        return None
    else:
        description = soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > p')
        if description:
            setattr(prod, "description", description.text)

        else:
            setattr(prod, "description", soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').next_sibling)
        setattr(prod, "product_photo", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-4.product-image > img').get_attribute("src"))
        setattr(prod, "category", "Декоративні покриття")
        if soup.find("a", string="Опис продукту"):
            div = soup.select_one('#tab1 > div > div.row > div')

            active_field = None

            for e in div.contents:
                if isinstance(e, Tag):
                    if e.name == 'h3' and e.text == 'Властивості':
                        active_field = 'vlastivosti'
                        continue
                    elif e.name == 'h3' and (e.text == 'Колірні відтінки' or e.text == 'Віддітки фарб'):
                        active_field = 'color_shade'
                        continue
                    elif e.name == 'h3' and e.text == 'Упаковка/Розмір тари':
                        active_field = 'packing'
                        continue
                    elif e.name == 'h3' and e.text == 'Ступінь глянцю':
                        active_field = 'gloss'
                        continue
                    elif e.name == 'h3' and e.text == 'Щільність':
                        active_field = 'density'
                        continue
                    elif e.name == 'h3':
                        active_field = None

                    if active_field is not None:
                        if e.name == 'ul' or e.name == 'ol':
                            text = "\n".join([str(c).replace('<li>', '- ').replace("</li>", "") for c in e.contents])
                        else:
                            text = str(e)
                        text = text.replace('<p>', '').replace('</p>', '')
                        text = text.replace('<strong>', '').replace('</strong>', '')
                        text = text.replace('<sub>', '').replace('</sub>', '')
                        text = text.replace('<sup>', '').replace('</sup>', '')
                        text = text.replace('<br/>', '\n')
                        text = text.replace('\n\n\n', '\n\n')
                        try:
                            text = text[:text.index('<table')] + text[text.index('</table>') + 8:]
                        except ValueError as ex:
                            pass
                        if getattr(prod, active_field):
                            text = '\n' + text
                        value = getattr(prod, active_field)
                        value = value if value is not None else ""
                        setattr(prod, active_field, value + text)
                if isinstance(e, NavigableString) and e is not None and active_field is not None:
                    value = getattr(prod, active_field)
                    value = value if value is not None else ""
                    setattr(prod, active_field, value + '\n' + str(e))
            ti = soup.select_one('#tab4 > div > div > div > ul > li > a')
            if ti is not None:
                setattr(prod, "ti", base_url + "/" + ti.get("href"))
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

        return prod


def parse_heat_systems(prod, browser):
    setattr(prod, "name", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').text)
    logger.info(f"Parsing {prod.name}")
    temp = DBSession.query(ProductHeatSystems).filter(ProductHeatSystems.name == prod.name).first()
    if temp:
        logger.info("Object already exist")
        return None
    else:
        description = soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > p')
        if description:
            setattr(prod, "description", description.text)

        else:
            setattr(prod, "description", soup.select_one('div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-5 > h1').next_sibling)
        setattr(prod, "product_photo", browser.find_element(By.CSS_SELECTOR, 'div > div.container.productContent > div > div.col-xs-12.col-sm-6.col-md-4.product-image > img').get_attribute("src"))
        if soup.find("a", string="Опис продукту"):
            div = soup.select_one('#tab1 > div > div.row > div')

            active_field = None

            for e in div.contents:
                if isinstance(e, Tag):
                    if e.name == 'h3' and e.text == 'Властивості':
                        active_field = 'vlastivosti'
                        continue
                    elif e.name == 'h3' and (e.text == 'Колірні відтінки' or e.text == 'Віддітки фарб'):
                        active_field = 'color_shade'
                        continue
                    elif e.name == 'h3' and e.text == 'Упаковка/Розмір тари':
                        active_field = 'packing'
                        continue
                    elif e.name == 'h3' and e.text == 'Ступінь глянцю':
                        active_field = 'gloss'
                        continue
                    elif e.name == 'h3' and e.text == 'Щільність':
                        active_field = 'density'
                        continue
                    elif e.name == 'h3':
                        active_field = None

                    if active_field is not None:
                        if e.name == 'ul' or e.name == 'ol':
                            text = "\n".join([str(c).replace('<li>', '- ').replace("</li>", "") for c in e.contents])
                        else:
                            text = str(e)
                        text = text.replace('<p>', '').replace('</p>', '')
                        text = text.replace('<strong>', '').replace('</strong>', '')
                        text = text.replace('<sub>', '').replace('</sub>', '')
                        text = text.replace('<sup>', '').replace('</sup>', '')
                        text = text.replace('<br/>', '\n')
                        text = text.replace('\n\n\n', '\n\n')
                        try:
                            text = text[:text.index('<table')] + text[text.index('</table>') + 8:]
                        except ValueError as ex:
                            pass
                        if getattr(prod, active_field):
                            text = '\n' + text
                        value = getattr(prod, active_field)
                        value = value if value is not None else ""
                        setattr(prod, active_field, value + text)
                if isinstance(e, NavigableString) and e is not None and active_field is not None:
                    value = getattr(prod, active_field)
                    value = value if value is not None else ""
                    setattr(prod, active_field, value + '\n' + str(e))
            ti = soup.select_one('#tab4 > div > div > div > ul > li > a')
            if ti is not None:
                setattr(prod, "ti", base_url + "/" + ti.get("href"))
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 'w')

        return prod


if __name__ == '__main__':

    if not DBSession.query(ProductCategory).filter(ProductCategory.name == "Ґрунтовки").first():
        logger.info("Categories parsed")
        category1 = ProductCategory()
        category2 = ProductCategory()
        category3 = ProductCategory()
        category4 = ProductCategory()
        category1.name = "Ґрунтовки"
        DBSession.add(category1)
        category2.name = "Фарби"
        DBSession.add(category2)
        category3.name = "Декоративні покриття"
        DBSession.add(category3)
        category4.name = "Шпаклівки та штукатурки"
        DBSession.add(category4)
        DBSession.commit()
    else:
        logger.info("Categories selected")

    options = webdriver.ChromeOptions()
    if HEADLESS:
        options.add_argument('headless')
        options.add_argument('window-size=1366x768')
    browser = Chrome(os.path.join(RESOURCES_FOLDER, 'chromedriver'), chrome_options=options)
    print(browser.get_window_size())
    if UPDATE_LINKS:
        links = get_links(browser, os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE))
    else:
        links = get_collected_data(os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE))['links']

    categories = []
    for link in links:
        browser.find_element_by_tag_name('body').send_keys(Keys.CONTROL + 't')
        browser.get(link)
        logger.info(link)

        # wait = WebDriverWait(browser, 10)
        # wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'div.product-rootline.hidden-sm.hidden-xs > ul > li:nth-child(3) > a')))

        r = requests.get(browser.current_url)
        soup = BeautifulSoup(r.text, 'html.parser')
        element = soup.select_one('div.product-rootline.hidden-sm.hidden-xs > ul')
        if element is None:
            continue
        category_bars = element.findChildren(recoursive=False)
        res = None
        for bar in category_bars:
            if "штукатурки" in bar.text and UPDATE_P_AND_P:
                prod = ProductPuttyAndPlaster()
                res = parse_putty_and_plaster(prod, browser)
            elif "Фасадні фарби" in bar.text and UPDATE_PAINTS:
                prod = ProductPaints()
                setattr(prod, "category", "Фасадні фарби")
                res = parse_paints(prod, browser)
            elif "Інтер'єрні фарби" in bar.text and UPDATE_PAINTS:
                prod = ProductPaints()
                setattr(prod, "category", "Інтер'єрні фарби")
                res = parse_paints(prod, browser)
            elif "Ґрунтовки" in bar.text and UPDATE_FIRST_COATS:
                prod = ProductFirstCoats()
                res = parse_first_coats(prod, browser)
            elif "Декоративні покриття" in bar.text and UPDATE_DECORATIVE:
                prod = ProductDecorativeCoatings()
                res = parse_decorative_coatings(prod, browser)
            elif "Система Capatect STANDARD" in bar.text and UPDATE_HEAT_SYSTEMS:
                str_bars = ''.join(x.text for x in category_bars)
                if "Ґрунтовки" not in str_bars and "Фасадні фарби" not in str_bars and "Фасадні штукатурки" not in str_bars:
                    prod = ProductHeatSystems()
                    setattr(prod, "category", "Capatect STANDARD")
                    res = parse_heat_systems(prod, browser)
            elif "Система Capatect CLASSIC" in bar.text and UPDATE_HEAT_SYSTEMS:
                str_bars = ''.join(x.text for x in category_bars)
                if "Ґрунтовки" not in str_bars and "Фасадні фарби" not in str_bars and "Фасадні штукатурки" not in str_bars:
                    prod = ProductHeatSystems()
                    setattr(prod, "category", "Capatect CLASSIC")
                    res = parse_heat_systems(prod, browser)
            elif "Система Capatect LONGLIFE" in bar.text and UPDATE_HEAT_SYSTEMS:
                str_bars = ''.join(x.text for x in category_bars)
                if "Ґрунтовки" not in str_bars and "Фасадні фарби" not in str_bars and "Фасадні штукатурки" not in str_bars:
                    prod = ProductHeatSystems()
                    setattr(prod, "category", "Capatect LONGLIFE")
                    res = parse_heat_systems(prod, browser)
            elif "Система Capatect CARBON" in bar.text and UPDATE_HEAT_SYSTEMS:
                str_bars = ''.join(x.text for x in category_bars)
                if "Ґрунтовки" not in str_bars and "Фасадні фарби" not in str_bars and "Фасадні штукатурки" not in str_bars:
                    prod = ProductHeatSystems()
                    setattr(prod, "category", "Capatect CARBON")
                    res = parse_heat_systems(prod, browser)
            if res is not None:
                DBSession.add(res)
                DBSession.commit()

    browser.close()
