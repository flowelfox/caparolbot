import json
import logging
import os
from time import sleep

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import Chrome

from src.models import DBSession, ProductFirstCoats, ProductPaints, ProductPuttyAndPlaster, ProductDecorativeCoatings
from src.settings import RESOURCES_FOLDER

COLLECTED_DATA_FILE_RU = 'ti_links_ru.json'
COLLECTED_DATA_FILE_UA = 'ti_links_ua.json'
UPDATE_LINKS = True
HEADLESS = True

base_url_ua = 'http://www.caparol.ua'
base_url_ru = 'http://www.caparol.ua/ua-ru'

logger = logging.getLogger(__name__)


def get_links(browser, file_path, lang):
    """Parse all product links"""
    if lang == "ru":
        base_url = base_url_ru
        browser.get(base_url + "/produkty/alfavitnyi-ukazatel.html")
    else:
        base_url = base_url_ua
        browser.get(base_url + "/produkti/alfavitnii-vkazivnik.html")

    browser.execute_script('window.results = [];')
    script = """
    let pimIDS = %product_id%;
    let pname = "%product_name%";
    $.ajax({
                type: "POST",
                url: window.location.pathname,
                data: {
                    eID: "getproductdata", request : {
                        action: "getCategory", arguments: {
                            getproduct: pimIDS
                        }
                    }
                },
                dataType: "html",
                success: function(result) {
                    window.results.push(result + '<br><i>' + pname + '</i>');
                },
                error: function(error) {
                    window.results.push(error);
                }
            });
    """

    ids = [(title.get_attribute("id"), title.text) for title in browser.find_elements_by_xpath("//h4[contains(@class, 'panel-title')]")]
    names = [name.text for name in browser.find_elements_by_xpath("//h4[contains(@class, 'panel-title')]")]

    for id, name in ids:
        browser.execute_script(script.replace('%product_id%', id).replace("%product_name%", name))

    data = []
    while len(data) < len(ids):
        data = browser.execute_script('return window.results')

        sleep(1)
        print(f"Collected {len(data)} results")

    links = []
    for html in data:
        if lang == "ru":
            soup = BeautifulSoup(html, 'html.parser')
            ti_link = soup.find('a', string='Техническая информация')
            if ti_link:
                links.append([base_url_ua + '/' + ti_link.get('href'), soup.find('i').text])

        else:
            soup = BeautifulSoup(html, 'html.parser')
            ti_link = soup.find('a', string='Технічна інформація')
            if ti_link:
                links.append([base_url + '/' + ti_link.get('href'), soup.find('i').text])

    write_collected_data({'links': links}, file_path)
    return links


def write_collected_data(new_data: dict, file_path=COLLECTED_DATA_FILE_UA):
    """Write dict to file"""
    data = get_collected_data(file_path)
    data.update(new_data)

    with open(file_path, 'w') as f:
        json.dump(data, f)


def get_collected_data(file_path):
    """Get all data which was saved to file"""
    if not os.path.exists(file_path):
        return {}
    with open(file_path, 'r') as f:
        data = json.load(f)
    return data


if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    if HEADLESS:
        options.add_argument('headless')
        options.add_argument('window-size=1366x768')
    browser = Chrome(os.path.join(RESOURCES_FOLDER, 'chromedriver'), chrome_options=options)
    products1 = DBSession.query(ProductFirstCoats).filter(ProductFirstCoats.language_code is not None).all()
    products2 = DBSession.query(ProductPaints).filter(ProductPaints.language_code is not None).all()
    products3 = DBSession.query(ProductPuttyAndPlaster).filter(ProductPuttyAndPlaster.language_code is not None).all()
    products4 = DBSession.query(ProductDecorativeCoatings).filter(ProductDecorativeCoatings.language_code is not None).all()
    if UPDATE_LINKS:
        ti_links_ru = get_links(browser, os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE_RU), "ru")
        ti_links_ua = get_links(browser, os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE_UA), "ua")
    else:
        ti_links_ru = get_collected_data(os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE_RU))['links']
        ti_links_ua = get_collected_data(os.path.join(RESOURCES_FOLDER, COLLECTED_DATA_FILE_UA))['links']

    for all_products in [products1, products2, products3, products4]:

        for prod in all_products:
            if prod.language_code == "ru":
                ti_links = ti_links_ru
                lang = "ru"
            else:
                ti_links = ti_links_ua
                lang = "uk"

            for link in ti_links:
                if link[1] == prod.name:
                    logger.info(link)
                    if lang == "ru":
                        setattr(prod, "ti", link[0])
                    else:
                        setattr(prod, "ti", link[0])
                    DBSession.add(prod)
                    DBSession.commit()

    browser.close()
