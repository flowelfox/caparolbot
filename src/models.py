import datetime

from botmanlib.models import BaseUser, Database, UserSessionsMixin, BaseUserSession
from sqlalchemy import Column, Integer, String, DateTime, Boolean, DDL, event, ForeignKey, Table, BigInteger
from sqlalchemy.orm import relationship

database = Database()
Base = database.Base

user_pollanswer_association_table = Table('pollanswer_user_association', Base.metadata,
                                          Column('user_id', Integer, ForeignKey('users.id')),
                                          Column('poll_answer_id', Integer, ForeignKey('poll_answers.id'))
                                          )

user_reaction_association_table = Table('reaction_user_association', Base.metadata,
                                        Column('user_id', Integer, ForeignKey('users.id')),
                                        Column('reaction_id', Integer, ForeignKey('reactions.id'))
                                        )


class User(Base, BaseUser, UserSessionsMixin):
    __tablename__ = 'users'

    desired_name = Column(String)
    answers = relationship("PollAnswer", back_populates="users", secondary=user_pollanswer_association_table)
    reactions = relationship("Reaction", back_populates="users", secondary=user_reaction_association_table)


class UserSession(Base, BaseUserSession):
    __tablename__ = 'user_sessions'


class ProductCategory(Base):
    __tablename__ = 'product_categories'

    id = Column(Integer, primary_key=True)

    name = Column(String)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)


class ProductPuttyAndPlaster(Base):
    __tablename__ = 'putty_and_plaster'

    id = Column(Integer, primary_key=True)
    hide = Column(Boolean, default=True)
    language_code = Column(String)
    name = Column(String)
    product_photo = Column(String)
    category = Column(String)
    additional_category = Column(String)
    description = Column(String)
    vlastivosti = Column(String)  # характеристики
    density = Column(String)  # Щільність
    consumption = Column(String)  # Расход
    color_shade = Column(String)  # Колірні відтінки
    gloss = Column(String)  # Ступінь глянцю
    packing = Column(String)  # Упаковка/Розмір тари
    grain = Column(String)  # Зернистость
    structure = Column(String)  # Структура
    ti = Column(String)  # техническая информация

    creating_date = Column(DateTime, default=datetime.datetime.utcnow)
    update_date = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    # category = relationship("ProductCategory", backref=backref('products', lazy='joined', cascade='all,delete', uselist=True), foreign_keys=[category_id])


class ProductPaints(Base):
    __tablename__ = 'paints'

    id = Column(Integer, primary_key=True)
    hide = Column(Boolean, default=True)
    language_code = Column(String)
    name = Column(String)
    product_photo = Column(String)
    category = Column(String)
    additional_category = Column(String)
    description = Column(String)
    vlastivosti = Column(String)  # характеристики
    density = Column(String)  # Щільність
    consumption = Column(String)  # Расход
    color_shade = Column(String)  # Колірні відтінки
    gloss = Column(String)  # Ступінь глянцю
    packing = Column(String)  # Упаковка/Розмір тари
    ti = Column(String)  # техническая информация

    creating_date = Column(DateTime, default=datetime.datetime.utcnow)
    update_date = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)


class ProductFirstCoats(Base):
    __tablename__ = 'first_coats'
    id = Column(Integer, primary_key=True)
    hide = Column(Boolean, default=True)
    language_code = Column(String)
    name = Column(String)
    product_photo = Column(String)
    category = Column(String)
    additional_category = Column(String)
    description = Column(String)
    vlastivosti = Column(String)  # характеристики
    density = Column(String)  # Щільність
    consumption = Column(String)  # Расход
    color_shade = Column(String)  # Колірні відтінки
    gloss = Column(String)  # Ступінь глянцю
    packing = Column(String)  # Упаковка/Розмір тари
    ti = Column(String)  # техническая информация

    creating_date = Column(DateTime, default=datetime.datetime.utcnow)
    update_date = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)


class ProductDecorativeCoatings(Base):
    __tablename__ = 'decorative_coats'
    id = Column(Integer, primary_key=True)
    hide = Column(Boolean, default=True)
    language_code = Column(String)
    name = Column(String)
    product_photo = Column(String)
    category = Column(String)
    additional_category = Column(String)
    description = Column(String)
    vlastivosti = Column(String)  # Особенности
    density = Column(String)  # Щільність
    consumption = Column(String)  # Расход
    color_shade = Column(String)  # Колірні відтінки
    gloss = Column(String)  # Ступінь глянцю
    packing = Column(String)  # Упаковка/Розмір тари
    ti = Column(String)  # техническая информация

    creating_date = Column(DateTime, default=datetime.datetime.utcnow)
    update_date = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)


class ProductHeatSystems(Base):
    __tablename__ = 'heat_systems'
    id = Column(Integer, primary_key=True)
    hide = Column(Boolean, default=True)
    language_code = Column(String)
    name = Column(String)
    product_photo = Column(String)
    category = Column(String)
    additional_category = Column(String)
    description = Column(String)
    vlastivosti = Column(String)  # Особенности
    density = Column(String)  # Щільність
    consumption = Column(String)  # Расход
    color_shade = Column(String)  # Колірні відтінки
    gloss = Column(String)  # Ступінь глянцю
    packing = Column(String)  # Упаковка/Розмір тари
    ti = Column(String)  # техническая информация

    creating_date = Column(DateTime, default=datetime.datetime.utcnow)
    update_date = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)


class Poll(Base):
    __tablename__ = 'polls'
    id = Column(Integer, primary_key=True)
    active = Column(Boolean, default=True, nullable=False)
    question = Column(String, nullable=False)
    answers = relationship("PollAnswer", back_populates="poll", cascade="all, delete-orphan")
    sent = Column(Boolean, default=False, nullable=False)
    messages = relationship("PollMessage", back_populates="poll")

    create_date = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)


class PollAnswer(Base):
    __tablename__ = 'poll_answers'
    id = Column(Integer, primary_key=True)
    text = Column(String, nullable=False)
    poll_id = Column(Integer, ForeignKey('polls.id'), nullable=False)
    poll = relationship("Poll", back_populates="answers")
    users = relationship("User", back_populates="answers", secondary=user_pollanswer_association_table)


class PollMessage(Base):
    __tablename__ = 'poll_messages'

    id = Column(Integer, primary_key=True)
    message_id = Column(BigInteger, nullable=False)
    chat_id = Column(BigInteger, nullable=False)

    poll_id = Column(Integer, ForeignKey('polls.id'), nullable=False)
    poll = relationship("Poll", back_populates="messages")



class Distribution(Base):
    __tablename__ = 'distributions'
    id = Column(Integer, primary_key=True)
    reactions = relationship("Reaction", back_populates="distribution", cascade="all, delete-orphan")
    messages = relationship("DistributionMessage", back_populates="distribution")
    create_date = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)


class Reaction(Base):
    __tablename__ = 'reactions'
    id = Column(Integer, primary_key=True)
    text = Column(String, nullable=False)
    distribution_id = Column(Integer, ForeignKey('distributions.id'), nullable=False)
    distribution = relationship("Distribution", back_populates="reactions")
    users = relationship("User", back_populates="reactions", secondary=user_reaction_association_table)


class DistributionMessage(Base):
    __tablename__ = 'distribution_messages'

    id = Column(Integer, primary_key=True)
    message_id = Column(BigInteger, nullable=False)
    chat_id = Column(BigInteger, nullable=False)

    distribution_id = Column(Integer, ForeignKey('distributions.id'), nullable=False)
    distribution = relationship("Distribution", back_populates="messages")


event.listen(
    ProductPaints.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)
event.listen(
    ProductFirstCoats.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)
event.listen(
    ProductPuttyAndPlaster.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)
event.listen(
    ProductDecorativeCoatings.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)
event.listen(
    ProductHeatSystems.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)

"""
ALTER SEQUENCE paints_id_seq restart with 10000;
ALTER SEQUENCE putty_and_plaster_id_seq restart with 10000;
ALTER SEQUENCE first_coats_id_seq restart with 10000;
ALTER SEQUENCE decorative_coats_id_seq restart with 10000;
ALTER SEQUENCE heat_systems_id_seq restart with 10000;
"""

DBSession = database.create_session("DBSession")
BlockSession = database.create_session("BlockSession")
JobSession = database.create_session("JobSession")
Session = database.sessionmaker()
