"""Added polls

Revision ID: 9c0eb297594b
Revises: e188b163950e
Create Date: 2020-09-04 16:54:54.946470

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9c0eb297594b'
down_revision = 'e188b163950e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('polls',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('active', sa.Boolean(), nullable=False),
    sa.Column('question', sa.String(), nullable=False),
    sa.Column('create_date', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('poll_answers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('text', sa.String(), nullable=False),
    sa.Column('poll_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['poll_id'], ['polls.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('pollanswer_user_association',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('poll_answer_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['poll_answer_id'], ['poll_answers.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('pollanswer_user_association')
    op.drop_table('poll_answers')
    op.drop_table('polls')
    # ### end Alembic commands ###
