"""Added reactions

Revision ID: 2fd57c41997d
Revises: 00921f929c99
Create Date: 2020-09-08 15:19:13.899454

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2fd57c41997d'
down_revision = '00921f929c99'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('distributions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('create_date', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('distribution_messages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('message_id', sa.BigInteger(), nullable=False),
    sa.Column('chat_id', sa.BigInteger(), nullable=False),
    sa.Column('distribution_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['distribution_id'], ['distributions.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('reactions',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('text', sa.String(), nullable=False),
    sa.Column('distribution_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['distribution_id'], ['distributions.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('reaction_user_association',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('reaction_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['reaction_id'], ['reactions.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('reaction_user_association')
    op.drop_table('reactions')
    op.drop_table('distribution_messages')
    op.drop_table('distributions')
    # ### end Alembic commands ###
